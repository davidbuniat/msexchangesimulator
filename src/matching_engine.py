'''
@authors: Davit Buniatyan
@organization: UCL
'''

from src.Book.book import Book
from src.Book.order import Order
from __builtin__ import True
from report import Report
import threading
__docformat__ = 'epytext'

class MatchingEngine:
    """
    Matching Engine class that processes and executes the orders.
    @type self.books: Dictionary
    @param self.books: The dictionary of the book 
    @type self.reporter: Report
    @param self.reporter: The Report class for sending reports 
    @type self.lock: threading.RLock
    @param self.lock: the lock for the Matching Engine for avoiding concurrent bugs
    """
    
    def __init__(self):
        self.books = {}
        self.reporter = Report()
        self.lock = threading.RLock()

    def add_book(self, book):
        """
        Adds the book to the dictionary
        @type book: Book
        @param book: The book added to the dictionary of books 
        """
        self.books[book.instrument_ID] = book
        
    def create_new_book(self, instrument_id):
        """
        Creates an empty book using instrument id and adds to the Dictionary
        @type instrument_id: String
        @param instrument_id: The Instrument ID from order
        """
        book = Book(instrument_id)
        self.add_book(book)
        return book
        
    def get_book(self, Instrument_ID):
        """
        Returns the book of the specified Instrument ID
        @type Instrument_ID: String
        @param Instrument_ID: The Instrument ID from order
        @rtype: Book
        @return: The book of the specified Instrument
        """
        return self.books[Instrument_ID]
    
    def get_books(self):
        """
        Returns the dictionary of books
        @rtype: Book
        @return: Dictionary of Books 
        """
        return self.books
            
    def process_order(self, order, new=True):
        """
        Processing the order and deciding the function. If the book is not found it automatically creates one. It calls the function for specified execution type.
        Rejects if the order has been found.
        @type order: order
        @param order: The order which is going to be executed
        @type new: Boolean
        @param new: If the order is new to the matching engine. Default True
        """
        try: 
            book = self.books[order.getID()['InstrumentID']]
        except KeyError:
            book = self.create_new_book(order.getID()['InstrumentID'])
            
        if new:
            if book.get_order_by_client_order_id(order.getID()['ClientOrderID'], order.getOrder()['side']) != None: 
                order.getState()["order"] = 'REJECTED'
                order.getState()["exec_type"] = 'REJECTED'
                self.reporter.all(order)
                return 
            
            order.getState()["order"] = 'NEW'
            order.getState()["exec_type"] = 'NEW'
            self.reporter.all(order)
            


        book.lock_book()
        execOrderByType = { 'limit': self.process_limit,
                                'market': self.process_market,
                                'ioc': self.process_ioc,
                                'fok': self.process_fok
                            }
        type = order.getOrder()['type']
        if order.getOrder()['tif'] != 'day':
            type = order.getOrder()['tif']
              
        execOrderByType[type](order, book)
        book.unlock_book()
    
    
    def cancel_order_by_id(self, Instrument_ID, side, order_id, encoder):
        """ 
        Cancel the given order by its ID and other details. Finds the order and calls cancel_orer(order).
        @type Instrument_ID: String
        @param Instrument_ID: The Instrument ID of the order
        @type side: String
        @param side: The side of the order
        @type order_id: String
        @param order_id: The order id
        @type encoder: Encoder
        @param encoder: The encoder of the order for reporting
        @rtype: Boolean
        @return: If the order was successfully cancelled
        """
        
        order = self.books[Instrument_ID].get_order_by_id(order_id, side)
        if order == None:
            return False
        
        order.setEncoderID(encoder)
        return self.cancel_order(order)
         
         
    def cancel_order(self, order):
        """
        Cancel the given order
        @type order: Order
        @param order: order to cancel
        @rtype: Boolean
        @return: If the order was successfully cancelled
        """
        book = self.books[order.getID()['InstrumentID']]
        succeeded = book.remove_order(order)
        
        if succeeded:
            order.setState( 'CANCELLED', 'CANCELLED', 0, 0, 0)
            self.reporter.all(order)
        else:
            reason = 0 
            self.reporter.cancel(order, reason)
        return succeeded
    
    def replace_order_by_id(self, new_order, Instrument_ID, side, order_id):
        """ 
        Replace the given order by old order's ID and other details. Finds the order and calls order_orer(old_order, new_order).
        @type new_order: Order
        @param new_order: new Order that should replace the old one
        @type Instrument_ID: String
        @param Instrument_ID: The Instrument ID of the order
        @type side: String
        @param side: The side of the order
        @type order_id: String
        @param order_id: The order id
        @rtype: Boolean
        @return: If the order was successfully replaced
        """ 
        order = self.books[Instrument_ID].get_order_by_id(order_id, side)
        if order == None:
            return False
        new_order.setOrderType(order.getOrder()['type'])
        self.replace_order(order, new_order)
        return True
        
    def replace_order(self, old_order, new_order):
        """
        Replace the order. If the order is already pending replace, reject the replacement
        @type old_order: Order
        @param old_order: old order that will be replaced
        @type new_order: Order
        @param new_order: new order that will replace the old one
        @rtype: Boolean
        @return: If the order was successfully replaced
        """
        book = self.books[old_order.getID()['InstrumentID']]
        

        if old_order.getState()["exec_type"] == "PENDING REPLACE":
            reason = 2
            self.reporter.cancel(new_order, reason)
            return
        
        book.lock_book()
        
        old_order.getState()["exec_type"] = "PENDING REPLACE"
        expired = self.books[old_order.getID()['InstrumentID']].is_expired(old_order)

        if expired:
            succeeded = True
            if old_order.getOrder()["quantity"] >= new_order.getOrder()["quantity"]:
                succeeded = False
        else:
            succeeded = self.books[old_order.getID()['InstrumentID']].remove_order(old_order)
        
        if succeeded:  
            leaves_qty = new_order.getOrder()["quantity"]-old_order.getOrder()["quantity"] + old_order.getState()["leaves_qty"]

            if leaves_qty<=0:
                #Expired order
                new_order.getOrder()["quantity"] -= leaves_qty
                leaves_qty=0
                new_order.setState("FILLED", "REPLACED", leaves_qty, 0,0)
                self.reporter.all(new_order)
            elif old_order.getOrder()["quantity"] == old_order.getState()["leaves_qty"]:
                #Process New Order
                new_order.setState("NEW", "REPLACED", leaves_qty, 0,0)
                self.process_order(new_order, False) 
                self.reporter.all(new_order)
            else: 
                #Process Replaced order
                new_order.setState("PARTIALLY", "REPLACED", leaves_qty, 0,0)
                self.process_order(new_order, False) 
                self.reporter.all(new_order)
        else:
            reason = 0
            self.reporter.cancel(new_order, reason)
            
        book.unlock_book()    
                 
    def process_limit(self, order, book):
        """
        Process limit type order in the specified book
        @type order: Order
        @param order: Order that is going to be executed
        @type book: Book
        @param book: The book where the order will be executed
        """
        side = order.getOrder()['side']

        if  (len(book.get_list_not(side)) == 0):
            book.add_order(order)
            return
        
        if (not self.can_be_matched(order, book.get_list_not(side)[0])):
            book.add_order(order)
            return
        
        while (order.getState()['leaves_qty'] > 0):
            opposite_sider = book.get_list_not(side)[0]
            
            if  not self.can_be_matched(order, opposite_sider):
                book.add_order(order)
                break

            elif order.getState()['leaves_qty'] > opposite_sider.getState()['leaves_qty']:
                executed_qty = opposite_sider.getState()['leaves_qty']
                order.getState()['leaves_qty'] -= executed_qty
                opposite_sider.getState()['leaves_qty'] -= executed_qty
                   
                order.setState('PARTIALLY', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])  
                self.reporter.all(order)
                opposite_sider.setState('FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price']) 
                self.reporter.all(opposite_sider)
                
                book.remove_order(opposite_sider)
                
            elif order.getState()['leaves_qty'] == opposite_sider.getState()['leaves_qty']:
                executed_qty = order.getState()['leaves_qty']
                order.getState()['leaves_qty'] = 0 
                opposite_sider.getState()['leaves_qty'] = 0
                
                order.setState('FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                
                book.remove_order(order)
                book.remove_order(opposite_sider)
                break
                
            elif order.getState()['leaves_qty'] < opposite_sider.getState()['leaves_qty']:
                executed_qty = order.getState()['leaves_qty']
                opposite_sider.getState()['leaves_qty'] -= executed_qty
                order.getState()['leaves_qty'] -= executed_qty
                 
                order.setState('FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('PARTIALLY', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                book.remove_order(order)        
                break
                
            if (len(book.get_list_not(side)) == 0):
                book.add_order(order)
                 
                break
        return
       
    def can_be_matched(self, order_1, order_2):
        """
        Check if first order's price is better, thus it could be matched.
        @type order_1: Order
        @param order_1: first order
        @type order_2: Order
        @param order_2: second order
        @rtype: Boolean
        @return: if the first order's price is better.
        """
        if order_1.ord['side'] != order_2.ord['side']:
            side = order_1.ord['side']
        else:
            raise NameError('input sides are equal')
            return False
        
        if (order_1.ord['price'] <= order_2.ord['price']) & (side == 'sell'):
            return True
        if (order_1.ord['price'] >= order_2.ord['price']) & (side == 'buy'):
            return True
        
        return False 
    
    def process_market(self, order, book):
        """
        Process market type order in the specified book
        @type order: Order
        @param order: Order that is going to be executed
        @type book: Book
        @param book: The book where the order will be executed
        """
        side = order.getOrder()['side']
        
        if len(book.get_list_not(side)) == 0:
            book.add_order(order)
            return
        
        while (order.getState()['leaves_qty'] > 0):
            opposite_sider = book.get_list_not(side)[0]
            if order.getState()['leaves_qty'] > opposite_sider.getState()['leaves_qty']:
                
                executed_qty = opposite_sider.getState()['leaves_qty']
                order.getState()['leaves_qty'] -= executed_qty
                opposite_sider.getState()['leaves_qty'] -= executed_qty
                
                order.setState('PARTIALLY', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                
                book.remove_order(opposite_sider)
                
                
            elif order.getState()['leaves_qty'] == opposite_sider.getState()['leaves_qty']:

                executed_qty = order.getState()['leaves_qty']
                order.getState()['leaves_qty'] = 0 
                opposite_sider.getState()['leaves_qty'] = 0
                
                order.setState('FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                book.remove_order(order)
                book.remove_order(opposite_sider)
                
                break
            
            elif order.getState()['leaves_qty'] < opposite_sider.getState()['leaves_qty']:
                executed_qty = order.getState()['leaves_qty']
                opposite_sider.getState()['leaves_qty'] -= executed_qty
                order.getState()['leaves_qty'] -= executed_qty

                order.setState('FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('PARTIALLY', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                book.remove_order(order)
                break
                
            if (len(book.get_list_not(side)) == 0):
                book.add_order(order)
                break

        return
    
    def process_ioc(self, order, book):
        """
        Process ioc tif type order in the specified book
        @type order: Order
        @param order: Order that is going to be executed
        @type book: Book
        @param book: The book where the order will be executed
        """
        side = order.getOrder()['side']
  
        if len(book.get_list_not(side)) == 0:
            order.setState( 'CANCELLED', 'CANCELLED', order.getState()['leaves_qty'], 0, 0)
            self.reporter.all(order)
            book.remove_order(order)
            return
        
        while (order.getState()['leaves_qty'] > 0):
            opposite_sider = book.get_list_not(side)[0]
            if not self.can_be_matched(order, opposite_sider):
                order.setState( 'CANCELLED', 'CANCELLED', order.getState()['leaves_qty'], 0, 0)
                
                self.reporter.all(order)
                book.remove_order(order)
                break
            
            elif order.getState()['leaves_qty'] > opposite_sider.getState()['leaves_qty']:
                executed_qty = opposite_sider.getState()['leaves_qty']
                order.getState()['leaves_qty'] -= executed_qty
                opposite_sider.getState()['leaves_qty'] = executed_qty
                
                order.setState('PARTIALLY', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)

                book.remove_order(opposite_sider)
                # Status Change
            elif order.getState()['leaves_qty'] == opposite_sider.getState()['leaves_qty']:
                executed_qty = order.getState()['leaves_qty']
                order.getState()['leaves_qty'] = 0
                opposite_sider.getState()['leaves_qty'] = 0
                
                # Status Change
                order.setState( 'FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState( 'FILLED', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                
                book.remove_order(order)
                book.remove_order(opposite_sider)
                
                break
            
            elif order.getState()['leaves_qty'] < opposite_sider.getState()['leaves_qty']:
                executed_qty = order.getState()['leaves_qty']
                opposite_sider.getState()['leaves_qty'] -= executed_qty
                order.getState()['leaves_qty'] -= executed_qty
                 
                # Status Change
                order.setState('FILLED', 'TRADE', order.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(order)
                opposite_sider.setState('PARTIALLY', 'TRADE', opposite_sider.getState()['leaves_qty'], executed_qty, opposite_sider.ord['price'])
                self.reporter.all(opposite_sider)
                book.remove_order(order)
                break
                
            if (len(book.get_list_not(side)) == 0):
                order.setState( 'CANCELLED', 'CANCELLED', order.getState()['leaves_qty'], 0, 0)
                
                self.reporter.all(order)
                book.remove_order(order)
                return
        return
    
    def process_fok(self, order, book):
        """
        Process fok tif type order in the specified book
        @type order: Order
        @param order: Order that is going to be executed
        @type book: Book
        @param book: The book where the order will be executed
        """
        side = order.getOrder()['side']
 
        if len(book.get_list_not(side)) == 0:
            print('342')
            order.setState( 'CANCELLED', 'CANCELLED', order.getState()['leaves_qty'], 0, 0)
            self.reporter.all(order)
            book.remove_order(order)
            return
        
        index = 0
        pseudo_qty = order.getState()['leaves_qty']
        fill = False
        
        #Imaginery matching
        while pseudo_qty > 0:
            opposite_sider = book.get_list_not(side)[index]
            
            if not self.can_be_matched(order, opposite_sider):
                break
            elif pseudo_qty > opposite_sider.getState()['leaves_qty']:
                pseudo_qty -= opposite_sider.getState()['leaves_qty']
                index = index + 1
            elif pseudo_qty <= opposite_sider.getState()['leaves_qty']:
                fill = True
                break
            
            if (len(book.get_list_not(side)) == index):
                break
        #Real matching
        if fill:  
            self.process_ioc(order, book)
        else:
            order.setState( 'CANCELLED', 'CANCELLED', order.getState()['leaves_qty'], 0, 0)
            self.reporter.all(order)
            book.remove_order(order)
        return
    
    def fill_order_by_id(self, Instrument_ID, side, order_id, encoder):
        """ 
        Admin access to the order by its Instrument_ID, side, order_id.
        @type Instrument_ID: String
        @param Instrument_ID: The Instrument ID of the order
        @type side: String
        @param side: The side of the order
        @type order_id: String
        @param order_id: The order id
        @type encoder: Encoder
        @param encoder: The encoder of the order for reporting
        @rtype: Boolean
        @return: If the order was successfully filled
        """
        order = self.books[Instrument_ID].get_order_by_id(order_id, side)
        if order == None:
            return False
        
        order.setEncoderID(encoder)
        return self.fill_order(order)
         
         
    def fill_order(self, order):
        """ 
        Fill order
        @type order: Order
        @param order: The order that should be filled
        @rtype: Boolean
        @return: If the order was successfully filled
        """
        book = self.books[order.getID()['InstrumentID']]
        
        succeeded = book.remove_order(order)
        
        if succeeded:
            order.setState( 'FILLED', 'TRADE', order.getState()['leaves_qty'], 0, 0)
            self.reporter.all(order)
        return succeeded
        
    def get_order_by_id(self, order_id):
        """ 
        Return the order by id. 
        @type order_id: String
        @param order_id: The order id 
        @rtype: Order
        @return: The order itself. Returns none if the order was not set.
        """
        for instrumentID in self.books:
            book = self.get_book(instrumentID)
            
            orderSell = book.get_order_by_id(order_id, 'sell')
            orderBuy = book.get_order_by_id(order_id, 'buy')
            
            if orderSell != None:
                return orderSell
            if orderBuy != None:
                return orderBuy
        return None
    
    def fill_order_only_by_id(self, order_id):
        """ 
        fill the order by only id.
        @type order_id: String
        @param order_id: The id of the order
        @rtype: Boolean
        @return: If the order was successfully filled
        """
        order = self.get_order_by_id(order_id)
        if order == None:
            return False
        
        return self.fill_order(order)
    
    def cancel_order_only_by_id(self, order_id):
        """ 
        cancel the order by only id.
        @type order_id: String 
        @param order_id: The id of the order
        @rtype: Boolean
        @return: If the order was successfully cancelled
        """
        order = self.get_order_by_id(order_id)
        if order == None:
            return False
        return self.cancel_order(order)
        
        
