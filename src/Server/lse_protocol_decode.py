"""
@author: GarSing Chow
This module contains the readable server message parser for the native London Stock Exchange (Millenium) protocol. For more information on this specification please see the LSE document MIT203 - Native Trading Gateway.
"""
__docformat__ = 'epytext'
import struct
from src.Book.order import Order
from src.report import Report

class Decode():
    """
    This class is the decoder class that contains all the functions that handle message parsing from the procotol into the necessary function calls and variables.
    """ 
    
    def __init__(self, server, ME, Encoder=None):
        """
        Initialises the variables for the class.
        @type self.ME: Matching engine object
        @param self.ME: The matching engine for the server.
        @type self.Encoder: Encoder object
        @param self.Encoder: The encoder for the client.
        @type self.server: Server object
        @param self.server: The server for the client.
        """
        self.ME = ME
        self.Encoder = Encoder
        self.server = server
        self.reporter = Report()
        
    def side_interpret(self, side):
        """
        Returns a side value that the matching engine uses.
        @type side: int
        @param side: The side value from the message.
        @rtype: string
        @return: The side value the matching engine utilises.
        """
        if side == 1:
            side = 'buy'
        elif side == 2:
            side = 'sell'
        return side

    def read_header(self, header):
        """
        Takes a header of a message, unpacks the struct and returns the message length.
        @type header: string
        @param header: The header of a message.
        @rtype: int
        @return: The message length.
        """
        (mesg_start, self.mesg_length,
        self.mesg_type) = struct.unpack('<bhc', header)
        return self.mesg_length

    def set_message(self, message):
        """
        Sets the message of the class in order to be processed.
        @type self.message: string
        @param self.message: The message from the server.
        @rtype: string
        @return: The message.
        """
        self.message = message
        return message
    
    
    def __logon(self):
        """
        Creates the necessary data to compare the login recieved from the client. Unpacks the message from the client, compares the data and sends the relevent logon reply message, returning a status value.
        @rtype: int
        @return: The status of the login.
        """
        serv_packed = struct.pack('<25s25s', 'admin', 'password')
        serv_user, serv_passwd = struct.unpack('<25s25s', serv_packed)
        username, passwd, new_passwd, mesg_ver = struct.unpack('<25s25s25sB', self.message)
        if username == serv_user:
            if passwd == serv_passwd:
                self.Encoder.logon_reply(0)
                return 1
            else: 
                self.Encoder.logon_reply(1)
                return 0
        else:
            self.Encoder.logon_reply(4)
            return 0


    def __logon_reply(self):
        """
        Unpacks logon reply message string into useable variables and then prints all the data.
        """
        reject_code, passwd_expiry = struct.unpack('<i30s', self.message)
        print locals()

    def __logout(self):
        """
        Unpacks logout message string into useable variables.
        """
        reason, = struct.unpack('<20s',self.message)[0]
        
    def __heartbeat(self):
        """
        Empty function.
        """
        pass

    def __missed_mesg_req(self):
        """
        Unpacks missed message request message string into useable variables and then prints all the data.
        """
        app_id, last_mesg_seq_num = struct.unpack('<bi', self.message)
        print locals()

    def __missed_mesg_req_ack(self):
        """
        Unpacks missed message request acknowledgement message string into useable variables and then prints all the data.
        """
        resp_type, = struct.unpack('<B', self.message)[0]
        print locals()

    def __missed_mesg_report(self):
        """
        Unpacks missed message report message string into useable variables and then prints all the data.
        """
        resp_type, = struct.unpack('<B', self.message)[0]
        print locals()

    def __reject(self):
        """
        Unpacks reject message string into useable variables and then prints all the data.
        """
        code, reason, mesg_type, client_order_id = struct.unpack('<i30sc20s', self.message) 
        print locals()

    def __system_status(self):
        """
        Unpacks system status message string into useable variables and then prints all the data.
        """
        app_id, app_status = struct.unpack('<BB', self.message)
        print locals()

    def __new_order(self):
        """
        Unpacks new order message string into useable variables, creates a new order from the information and sends the order to the matching engine.
        """
        (client_order_id, trader_id, account, clearing_account, instrument_id,
        reserved1, reserved2, order_type, tif, expiry_date_time, side,
        order_qty, display_qty, limit_price, capacity, auto_cancel,
        order_sub_type, anonymity, stopped_price, passive_only_order,
        reserved3) = struct.unpack('<20s11s10sBibbBBIBiiqBBBBqB9s',
                                                  self.message)
        
        order1 = Order()
        order1.setEncoderID(self.Encoder)
        order1.newOrder(client_order_id, trader_id, instrument_id, order_type, tif, side, order_qty, limit_price, 'UCL')
        self.ME.process_order(order1)


    def __new_quote(self):
        """
        Unpacks new quote message string into useable variables and then prints all the data.
        """
        (client_order_id, trader_id, clearing_account, instrument_id,
        bid_price, bid_size, ask_price, ask_size, capacity, auto_cancel,
        reserved) = struct.unpack('<20s11sBiqiqiBB10s', self.message)
        print locals()

    def __order_cancel_replace_request(self):
        """
        Unpacks order cancel replace request message string into useable variables and then creates a new order from the information. Then it sends a replace order request to the matching engine with this order.
        """
        (client_order_id, original_client_order_id, order_id, instrument_id,
        reserved1, reserved2, expire_date_time, order_qty, display_qty,
        limit_price, account, tif, side, stopped_price, passive_only_order,
        reserved3) = struct.unpack('<20s20s12sibbIiiq10sbbqb9s',
                                                  self.message)
        order_new = Order()
        order_new.setEncoderID(self.Encoder)
        order_new.newOrder(client_order_id, 'unset', instrument_id, 'unset', tif, side, order_qty, limit_price, 'UCL')
        side = self.side_interpret(side)
        self.ME.replace_order_by_id(order_new, instrument_id, side, order_id)
        

    def __order_cancel_request(self):
        """
        Unpacks order cancel request message string into useable variables and then it sends a cancel order request to the matching engine with this information.
        """
        (client_order_id, original_client_order_id, order_id, 
        instrument_id, reserved1, reserved2, side, 
        reserved3) = struct.unpack('<20s20s12sibbB10s', self.message)
        
        side = self.side_interpret(side)
        self.ME.cancel_order_by_id(instrument_id, side, order_id, self.Encoder)

    def __order_mass_cancel_request(self):
        """
        Unpacks order mass cancel request message string into useable variables and then prints all the data.
        """
        (client_order_id, mass_cancel_request_type, instrument_id, 
        reserved1, reserved2, segment, order_sub_type, 
        reserved3) = struct.unpack('<20sBibb4sB10s', self.message)
        print locals()

    def __execution_report(self):
        """
        Unpacks execution report message string into useable variables and then prints all the data.
        """
        (app_id, sequence_no, execution_id, client_order_id, order_id,
        exec_type, execution_report_ref_id, order_status, order_reject_code, 
        executed_price, executed_qty, leaves_qty, container, display_qty, 
        instrument_id, restatement_reason, reserved1, side, reserved2, 
        counterparty, trade_liquidity_indicator, trade_match_id, transact_time, 
        reserved3, type_of_trade, capacity, price_differential, 
        public_order_id) = struct.unpack('<Bi12s20s12sc12sBiqiiBiiBbbQ11scQQ\
                                                           1sBBc12s', self.message)
        print locals()


    def __order_cancel_reject(self):
        """
        Unpacks order cancel reject message string into useable variables and then prints all the data.
        """
        (app_id, sequence_no, client_order_id, order_id, 
        cancel_reject_reason, transact_time, 
        reserved) = struct.unpack('<Bi20s12siQ10s', self.message)
        print locals()

    def __order_mass_cancel_report(self):
        """
        Unpacks order mass cancel report message string into useable variables and then prints all the data.
        """
        (app_id, sequence_no, client_order_id, mass_cancel_response,
        mass_cancel_request_reason, reserved1, transact_time, 
        reserved2) = struct.unpack('<Bi20sBiiQ10s', self.message)
        print locals()

    def __business_reject(self):
        """
        Unpacks business reject message string into useable variables and then prints all the data.
        """
        (app_id, sequence_no, reject_code, client_order_id, order_id, 
        transact_time, reserved) = struct.unpack('<Bii20s12sQ10s', self.message)
        print locals()

    def __get_order_by_ID(self):
        """
        Unpacks get order by ID message string into useable variables, strips the string of null characters, and calls the get order function from the matching engine using the order ID. If successful it calls the print order function in the reporter. A success or failure message is then sent to the client.
        """
        order_id, = struct.unpack('<12s', self.message)
        order_id = order_id.rstrip('\0')
        order = self.ME.get_order_by_id(order_id)
        if order != None:
            self.reporter.print_order(order)
            self.server.send_message(self.Encoder.create_header('T'))
        else:
            self.server.send_message(self.Encoder.create_header('t'))
            
    def __fill_order_by_ID(self):
        """
        Unpacks fill order by ID message string into useable variables, strips the string of null characters, and calls the fill order function from the matching engine using the order ID. If successful it calls the fill order function in the reporter. A success or failure message is then sent to the client.
        """
        order_id, = struct.unpack('<12s', self.message)
        order_id = order_id.rstrip('\0')
        status = self.ME.fill_order_only_by_id(order_id)
        
        if status == True:
            self.server.send_message(self.Encoder.create_header('T'))
        else: 
            self.server.send_message(self.Encoder.create_header('t'))
            
    def __cancel_order_by_ID(self):
        """
        Unpacks cancel order by ID message string into useable variables, strips the string of null characters, and calls the get order function from the matching engine using the order ID. If successful it calls the cancel order function in the reporter. A success or failure message is then sent to the client.
        """
        order_id, = struct.unpack('<12s', self.message)
        order_id = order_id.rstrip('\0')
        status = self.ME.cancel_order_only_by_id(order_id)
        
        if status == True:
            self.server.send_message(self.Encoder.create_header('T'))
        else: 
            self.server.send_message(self.Encoder.create_header('t'))
            
    def __get_all_books(self):
        """
        Calls the get books function from the matching engine. Then uses this to call the print list of books function in the reporter object. A success message is sent to the client.
        """
        self.reporter.print_list_of_books(self.ME.get_books())
        self.server.send_message(self.Encoder.create_header('T'))
        
    def __get_book(self):
        """
        Unpacks get book message string into useable variables, and calls the get book function from the matching engine using the instrument ID. If successful it calls the print book info function in the reporter. A success message is then sent to the client.
        """
        instrument_id, = struct.unpack('<i', self.message)
        book = self.ME.get_book(instrument_id)
        self.reporter.print_book_info(book)
        self.server.send_message(self.Encoder.create_header('T'))
        
    def __create_new_book(self):
        """
        Unpacks create new book message string into useable variables, and calls the create new book function from the matching engine using the instrument ID. A success message is then sent to the client.
        """
        instrument_id, = struct.unpack('<i', self.message)
        self.ME.create_new_book(instrument_id)
        self.server.send_message(self.Encoder.create_header('T'))

    def parse_message(self):
        """
        Function that checks the message type contained within the class, and calls the correct function to process the message, otherwise printing an error message.
        @rtype: int or None
        @return: Returns a status value or nothing.
        """
        if self.mesg_type == 'A':
            return self.__logon()
        elif self.mesg_type == 'B':
            self.__logon_reply()
        elif self.mesg_type == '5':
            self.__logout()
            return 1
        elif self.mesg_type == '0':
            self.__heartbeat()
        elif self.mesg_type == 'M':
            self.__missed_mesg_req()
        elif self.mesg_type == 'N':
            self.__missed_mesg_req_ack()
        elif self.mesg_type == 'P':
            self.__missed_mesg_report()
        elif self.mesg_type == '3': 
            self.__reject()
        elif self.mesg_type == 'n':
            self.__system_status()
        elif self.mesg_type == 'D':
            self.__new_order()
        elif self.mesg_type == 'F':
            self.__order_cancel_request()
        elif self.mesg_type == 'q':
            self.__order_mass_cancel_request()
        elif self.mesg_type == 'G':
            self.__order_cancel_replace_request()
        elif self.mesg_type == 'S':
            self.__new_quote()
        elif self.mesg_type == '8':
            self.__execution_report()
        elif self.mesg_type == '9':
            self.__order_cancel_reject()
        elif self.mesg_type == 'r':
            self.__order_mass_cancel_report()
        elif self.mesg_type == 'j':
            self.__business_reject()
        elif self.mesg_type == 'Z':
            self.__get_order_by_ID()
        elif self.mesg_type == 'Y':
            self.__fill_order_by_ID()
        elif self.mesg_type == 'X':
            self.__cancel_order_by_ID()
        elif self.mesg_type == 'W':
            self.__get_all_books()
        elif self.mesg_type == 'V':
            self.__get_book()
        elif self.mesg_type == 'U':
            self.__create_new_book()
        else: 
            print "This message type is unsupported"