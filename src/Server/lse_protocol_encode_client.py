"""
@author: GarSing Chow
This module encodes a message for the native London Stock Exchange (Millenium) protocol. For more information on this specification please see the LSE document MIT203 - Native Trading Gateway.
"""
__docformat__ = 'epytext'
import struct

class Encode:
    """
    This class is the encoder class that contains all the functions that handle message creation from the procotol.
    @type self.header: string
    @param self.header: The header of a message.
    @type self.mesg_type: char
    @param self.mesg_type: The message type of the message.
    """ 

    def logon(self):
        """
        Packs the data into a formatted struct then returns the encoded message and header for a logon message. 
        @rtype: string
        @return: Packed header and message.
        
        """
        self.header = struct.pack('<bhc', 2, 76, 'A')
        username = 'admin'
        passwd = 'password'
        new_passwd = ''
        mesg_ver = 1
        return self.header + struct.pack('<25s25s25sB', username, passwd,
                                                          new_passwd, mesg_ver)

    def __get_order_by_ID(self):
        """
        Gets an order ID from the user, packs the data into a formatted struct, then returns the encoded header and message for a get order by id message.
        @rtype: string
        @return: Packed header and message.
        """
        order_id = raw_input('Enter order ID: ')
        return self.header + struct.pack('<12s', order_id)
    
    def __fill_order_by_ID(self):
        """
        Gets an order ID from the user, packs the data into a formatted struct, then returns the encoded header and message for a fill order by id message.
        @rtype: string
        @return: Packed header and message.
        """
        order_id = raw_input('Enter order ID: ')
        return self.header + struct.pack('<12s', order_id)
    
    def __cancel_order_by_ID(self):
        """
        Gets an order ID from the user, packs the data into a formatted struct, then returns the encoded header and message for a cancel order by id message.
        @rtype: string
        @return: Packed header and message.
        """
        order_id = raw_input('Enter order ID: ')
        return self.header + struct.pack('<12s', order_id)
    
    def __get_all_books(self):
        """
        Returns the header, as there is no data required for this message.
        @rtype: string
        @return: Packed header.
        """
        return self.header
    
    def __get_book(self):
        """
        Gets an instrument ID from the user, packs the data into a formatted struct, then returns the encoded header and message for a get book message.
        @rtype: string
        @return: Packed header and message.
        """
        instrument_id = int(raw_input('Enter instrument ID: '))
        return self.header + struct.pack('<i', instrument_id)
    
    def __create_new_book(self):
        """
        Gets an instrument ID from the user, packs the data into a formatted struct, then returns the encoded header and message for a create new book message.
        @rtype: string
        @return: Packed header and message.
        """
        instrument_id = int(raw_input('Enter instrument ID: '))
        return self.header + struct.pack('<i', instrument_id)

    def __logon_reply(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a logon reply message.
        @rtype: string
        @return: Packed header and message.
        """
        reject_code = int(raw_input('Enter the reject code: '))
        passwd_expiry = raw_input('Enter the password expiry: ')
        return self.header + struct.pack('<i30s', reject_code, passwd_expiry)

    def __logout(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a logout message.
        @rtype: string
        @return: Packed header and message.
        """
        reason = raw_input('Enter the logout reason: ')
        return self.header + struct.pack('<20s', reason)

    def __heartbeat(self):
        """
        Returns the header, as there is no data required for this message.
        @rtype: string
        @return: Packed header.
        """
        return self.header

    def __missed_mesg_req(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a missed message request message.
        @rtype: string
        @return: Packed header and message.
        """
        app_id = int(raw_input('Enter the AppID:'))
        last_mesg_seq_num = int(raw_input('Enter the last recieved sequence number: '))
        return self.header + struct.pack('<bi', app_id, last_mesg_seq_num)

    def __missed_mesg_req_ack(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a missed message request acknowledgment message.
        @rtype: string
        @return: Packed header and message.
        """
        resp_type = int(raw_input('Enter the response type: '))
        return self.header + struct.pack('<B', resp_type)

    def __missed_mesg_report(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a missed message report message.
        @rtype: string
        @return: Packed header and message.
        """
        resp_type = int(raw_input('Enter the response type: '))
        return self.header + struct.pack('<B', resp_type)

    def __reject(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a reject message.
        @rtype: string
        @return: Packed header and message.
        """
        code = int(raw_input('Enter reject code: '))
        reason = raw_input('Enter reject reason: ')
        mesg_type = raw_input('Enter message type: ')
        client_order_id  = raw_input('Enter client order ID: ')
        return self.header + struct.pack('<i30sc20s', code, reason, mesg_type, client_order_id)

    def __system_status(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a system status message.
        @rtype: string
        @return: Packed header and message.
        """
        app_id = int(raw_input('Enter app ID: '))
        app_status = int(raw_input('Enter app status: '))
        return self.header + struct.pack('<BB', app_id, app_status)

    def new_order(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a new order message.
        @rtype: string
        @return: Packed header and message.
        """
        self.header = struct.pack('<bhc', 2, 93, 'D')
        trader_id = 'trader_id'
        account = 'account'
        clearing_account = 1
        reserved1 = 0
        reserved2 = 0
        expiry_date_time = 0
        capacity = 1
        auto_cancel = 0
        order_sub_type = 0
        anonymity = 0
        stopped_price = 0
        passive_only_order = 0
        reserved3 = ''
        instrument_id = int(raw_input('Enter instrument ID'))
        client_order_id = raw_input('Enter client order ID: ')
        order_type = int(raw_input('Enter order type: '))
        tif = int(raw_input('Enter time qualifier: '))
        side = int(raw_input('Enter side: '))
        order_qty = int(raw_input('Enter order quantity: '))
        display_qty = order_qty
        limit_price = int(raw_input('Enter limit price: '))

        return self.header + struct.pack('<20s11s10sBibbBBIBiiqBBBBqB9s', client_order_id,
                                    trader_id, account, clearing_account, instrument_id,
                                    reserved1, reserved2, order_type, tif, expiry_date_time, side,
                                    order_qty, display_qty, limit_price, capacity, auto_cancel,
                                    order_sub_type, anonymity, stopped_price, passive_only_order,
                                    reserved3)

    def __new_quote(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for a new quote message.
        @rtype: string
        @return: Packed header and message.
        """
        client_order_id = raw_input('Enter client order ID: ')
        trader_id = raw_input('Enter trader ID: ')
        clearing_account, = int(raw_input('Enter clearing account: '))
        instrument_id = int(raw_input('Enter instrument ID: '))
        bid_price = int(raw_input('Enter bid price: '))
        bid_size = int(raw_input('Enter bid price: '))
        ask_price = int(raw_input('Enter ask price: '))
        ask_size = int(raw_input('Enter ask size: '))
        capacity = int(raw_input('Enter capacity: '))
        auto_cancel = int(raw_input('Enter auto cancel code: '))
        reserved = ''
        return self.header + struct.pack('<20s11sBiqiqiBB10s', client_order_id, trader_id, 
                                                        clearing_account, instrument_id, bid_price, bid_size, 
                                                        ask_price, ask_size, capacity, auto_cancel, reserved)

    def __order_cancel_replace_request(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for an order cancel replace request message.
        @rtype: string
        @return: Packed header and message.
        """
        client_order_id = raw_input('Enter client order ID: ')
        original_client_order_id = raw_input('Enter original client order ID: ')
        order_id = raw_input('Enter order ID: ')
        instrument_id = int(raw_input('Enter instrument ID: '))
        reserved1 = 0
        reserved2 = 0
        expire_date_time = int(raw_input('Enter expiry date & time: '))
        order_qty = int(raw_input('Enter order quantity: '))
        display_qty = int(raw_input('Enter display quantity: '))
        limit_price = int(raw_input('Enter limit price: '))
        account = raw_input('Enter account ref: ')
        tif = int(raw_input('Enter time qualifier: '))
        side = int(raw_input('Enter side: '))
        stopped_price = int(raw_input('Enter stopped price: '))
        passive_only_order = int(raw_input('Enter passive only order code: '))
        reserved3 = ''
        return self.header + struct.pack('<20s20s12sibbIiiq10sbbqb9s', client_order_id, original_client_order_id, 
                                    order_id, instrument_id, reserved1, reserved2, expire_date_time, order_qty, 
                                    display_qty, limit_price, account, tif, side, stopped_price, passive_only_order,
                                    reserved3)

    def __order_cancel_request(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for an order cancel request message.
        @rtype: string
        @return: Packed header and message.
        """
        client_order_id = raw_input('Enter client order ID: ')
        original_client_order_id = raw_input('Enter original client order ID: ')
        order_id = raw_input('Enter order ID: ')
        instrument_id = int(raw_input('Enter instrument ID: '))
        reserved1 = 0
        reserved2 = 0
        side = int(raw_input('Enter side: '))
        reserved3 = ''
        return self.header + struct.pack('<20s20s12sibbB10s', client_order_id, original_client_order_id, order_id, 
                                    instrument_id, reserved1, reserved2, side, reserved3)

    def __order_mass_cancel_request(self):
        """
        Gets the necessary inputs from the user, packs the data into a formatted struct, then returns the encoded header and message for an order mass cancel request message.
        @rtype: string
        @return: Packed header and message.
        """
        client_order_id = raw_input('Enter client order ID: ')
        mass_cancel_request_type = int(raw_input('Enter mass cancel request type: '))
        instrument_id = int(raw_input('Enter instrument ID: '))
        reserved1 = 0
        reserved2 = 0
        segment = raw_input('Enter segment: ')
        order_sub_type = int(raw_input('Enter order sub type: '))
        reserved3 = ''
        return self.header + struct.pack('<20sBibb4sB10s', client_order_id, mass_cancel_request_type, instrument_id, 
                                    reserved1, reserved2, segment, order_sub_type, reserved3)

    def __create_message(self, mesg_type):
        """
        Function that checks the message type, and calls the correct function to create the message, otherwise printing an error message.
        @rtype: string
        @return: Packed header (and message).
        """
        if mesg_type == 'A':
            self.header = struct.pack('<bhc', 2, 76, mesg_type)
            return self.__logon()
        elif mesg_type == 'B':
            self.header = struct.pack('<bhc', 2, 34, mesg_type)
            return self.__logon_reply()
        elif mesg_type == '5':
            self.header = struct.pack('<bhc', 2, 20, mesg_type)
            return self.__logout()
        elif mesg_type == '0':
            self.header = struct.pack('<bhc', 2, 0, mesg_type)
            return self.__heartbeat()
        elif mesg_type == 'M':
            self.header = struct.pack('<bhc', 2, 5, mesg_type)
            return self.__missed_mesg_req()
        elif mesg_type == 'N':
            self.header = struct.pack('<bhc', 2, 1, mesg_type)
            return self.__missed_mesg_req_ack()
        elif mesg_type == 'P':
            self.header = struct.pack('<bhc', 2, 1, mesg_type)
            return self.__missed_mesg_report()
        elif mesg_type == '3': 
            self.header = struct.pack('<bhc', 2, 55, mesg_type)
            return self.__reject()
        elif mesg_type == 'n':
            self.header = struct.pack('<bhc', 2, 2, mesg_type)
            return self.__system_status()
        elif mesg_type == 'D':
            self.header = struct.pack('<bhc', 2, 93, mesg_type)
            return self.new_order()
        elif mesg_type == 'F':
            self.header = struct.pack('<bhc', 2, 69, mesg_type)
            return self.__order_cancel_request()
        elif mesg_type == 'q':
            self.header = struct.pack('<bhc', 2, 42, mesg_type)
            return self.__order_mass_cancel_request()
        elif mesg_type == 'G':
            self.header = struct.pack('<bhc', 2, 118, mesg_type)
            return self.__order_cancel_replace_request()
        elif mesg_type == 'S':
            self.header = struct.pack('<bhc', 2, 82, mesg_type)
            return self.__new_quote()
        elif mesg_type == 'Z':
            self.header = struct.pack('<bhc', 2, 12, mesg_type)
            return self.__get_order_by_ID()
        elif mesg_type == 'Y':
            self.header = struct.pack('<bhc', 2, 12, mesg_type)
            return self.__fill_order_by_ID()
        elif mesg_type == 'X':
            self.header = struct.pack('<bhc', 2, 12, mesg_type)
            return self.__cancel_order_by_ID()
        elif mesg_type == 'W':
            self.header = struct.pack('<bhc', 2, 0, mesg_type)
            return self.__get_all_books()
        elif mesg_type == 'V':
            self.header = struct.pack('<bhc', 2, 4, mesg_type)
            return self.__get_book()
        elif mesg_type == 'U':
            self.header = struct.pack('<bhc', 2, 4, mesg_type)
            return self.__create_new_book()
        else:
            print "This message type is unsupported"
            return self.make_message()

    def make_message(self):
        """
        Function that prints the information for the client, then gets the message type the user wants to send, and calls the function to create the message.
        @rtype: string
        @return: Packed header (and message).
        """
        print('----------------------------------------------------------------------')
        print('Stock Exchange Simulator Admin Client')
        print('Outputs from admin (*) options are shown in the Server window.\nChoose from the following options:')
        print('W: List all books in stock exchange.*\nV: View orders in a book.*\nU: Create a new book.*\nZ: View an order.*\nY: Fill an order.*\nX: Cancel an order.*\nD: Send a new Order.\n0: Recieve a message.')
        print('Advanced user\'s may also enter any MsgType as listed in the MIT203 document.')
        print('----------------------------------------------------------------------')
        self.mesg_type = raw_input('Please enter an option: ')
        return self.__create_message(self.mesg_type)