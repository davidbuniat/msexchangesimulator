"""
@author: GarSing Chow
This module encodes a client message for the native London Stock Exchange (Millenium) protocol. For more information on this specification please see the LSE document MIT203 - Native Trading Gateway.
"""
__docformat__ = 'epytext'
import struct
import sys

class Encode:
    """
    This class is the encoder class that contains all the functions that handle message creation from the procotol.
    @type self.header: string
    @param self.header: The header of a message.
    """ 
    
    def __init__(self, server=None):
        """
        Initialises the values of the Encode class.
        @type self.server: server object
        @param self.server: The server object for the client this encoder belongs to.
        """
        self.server = server
        
    def logon_reply(self, reject_code, passwd_expiry='0'):
        """
        Creates the correct header, packs the data into a formatted struct, then sends the encoded header and message for a logon reply message to the client.
        @type reject_code: string
        @param reject_code: The logon reject code.
        @type passwd_expiry: char
        @param passwd_expiry: The password expiry character.
        """
        self.header = struct.pack('<bhc', 2, 34, 'B')
        self.server.send_message(self.header + struct.pack('<i30s', reject_code, passwd_expiry))

    def __heartbeat(self):
        """
        Returns the header, as there is no data required for this message.
        @rtype: string
        @return: Packed header.
        """
        return self.header

    def __success(self):
        """
        Returns the header, as there is no data required for this message.
        @rtype: string
        @return: Packed header.
        """
        return self.header
    
    def __fail(self):
        """
        Returns the header, as there is no data required for this message.
        @rtype: string
        @return: Packed header.
        """
        return self.header

    def create_header(self, mesg_type):
        """
        Function that checks the message type, and calls the correct function to create the message, otherwise printing an error message.
        @type mesg_type: char
        @param mesg_type: Message type of the message.
        @rtype: string
        @return: Packed header (and message).
        """
        if mesg_type == 'B':
            self.header = struct.pack('<bhc', 2, 34, mesg_type)
            return self.logon_reply()
        elif mesg_type == '0':
            self.header = struct.pack('<bhc', 2, 0, mesg_type)
            return self.__heartbeat()
        elif mesg_type == 'T':
            self.header = struct.pack('<bhc', 2, 0, mesg_type)
            return self.__success()
        elif mesg_type == 't':
            self.header = struct.pack('<bhc', 2, 0, mesg_type)
            return self.__fail()
        else:
            print "This message type is unsupported"

    def order_status_interpret(self, a): 
        """
        Returns an order status value that the protocol uses.
        @type a: string
        @param a: The order status value from the matching engine.
        @rtype: int
        @return: The order status value the protocol utilises.
        """
        if a == 'NEW':
            a = 0
        elif a == 'PARTIALLY':
            a = 1
        elif a == 'FILLED':
            a = 2
        elif a == 'CANCELLED':
            a = 4
        elif a == 'REJECTED':
            a = 8
        return a
            
    def exec_type_interpret (self, a):
        """
        Returns an exec type value that the protocol uses.
        @type a: string
        @param a: The order status value from the matching engine.
        @rtype: char
        @return: The exec type value the protocol utilises.
        """
        if a == 'NEW':
            a = '0'
        elif a == 'EXPIRED':
            a = 'C'
        elif a == 'TRADE':
            a = 'F'
        elif a == 'CANCELLED':
            a = '4'
        elif a == 'REPLACED':
            a = '5'
        elif a == 'REJECTED':
            a = '8'
        return a
    
    def side_interpret(self, side):
        """
        Returns a side value that the protocol uses.
        @type side: string
        @param side: The side value from the matching engine.
        @rtype: int
        @return: The side value the protocol utilises.
        """
        if side == 'buy':
            side = 1
        elif side == 'sell':
            side = 2
        return side
        
    def send_execution_report(self, side, order_status, exec_type, leaves_qty, executed_qty, executed_price, order_id, client_order_id):
        """
        Packs the parameters and other data into a formatted struct, then sends the encoded header and message for an execution report message.
        @type side: string
        @param side: The side value from the matching engine.
        @type order_status: string
        @param order_status: The order status value from the matching engine.
        @type exec_type: string
        @param exec_type: The execution type from the matching engine.
        @type leaves_qty: int
        @param leaves_qty: The leaves quantity from the matching engine.
        @type executed_qty: int
        @param executed_qty: The executed quantity from the matching engine.
        @type order_id: int
        @param order_id: The executed price from the matching engine.
        @type client_order_id: string
        @param client_order_id: The order id from the matching engine.
        """
        side = self.side_interpret(side)
        order_status = self.order_status_interpret(order_status)
        exec_type = self.exec_type_interpret(exec_type)

        self.header = struct.pack('<bhc', 2, 147, '8')
        app_id = 1
        sequence_no = 1
        execution_id = 'undef'
        execution_report_ref_id = 'undef'
        order_reject_code = 1 
        container = 1
        display_qty = 1
        instrument_id = 1
        restatement_reason = 1
        reserved1 = 0
        reserved2 = 0 
        counterparty = 'undef'
        trade_liquidity_indicator = 'A'
        trade_match_id = 1
        transact_time = 0 
        reserved3 = '0'
        type_of_trade = 2
        capacity = 0
        price_differential = 'A' 
        public_order_id = 'undef'
        message = self.header + struct.pack('<Bi12s20s12sc12sBiqiiBiiBbbQ11scQQ1sBBc12s', app_id, sequence_no, execution_id, client_order_id, order_id,
                                            exec_type, execution_report_ref_id, order_status, order_reject_code, 
                                            executed_price, executed_qty, leaves_qty, container, display_qty, 
                                            instrument_id, restatement_reason, reserved1, side, reserved2, 
                                            counterparty, trade_liquidity_indicator, trade_match_id, transact_time, 
                                            reserved3, type_of_trade, capacity, price_differential, 
                                            public_order_id)
        try:
            self.server.send_message(message)
        except: 
            self.server.request.close()  
        
    def send_order_cancel_reject(self, client_order_id, order_id, reason):
        """
        Packs the parameters and other data into a formatted struct, then sends the encoded header and message for a send order cancel reject message.
        @type client_order_id: string
        @param client_order_id: The client order id from the matching engine.
        @type order_id: string
        @param order_id: The order id from the matching engine.
        @type reason: int
        @param reason: The cancel reject reason.
        """
        self.header = struct.pack('<bhc', 2, 59, '9')
        app_id = 1
        sequence_no = 1
        cancel_reject_reason = reason
        transact_time = 1
        reserved = ''
        message = self.header + struct.pack('<Bi20s12siQ10s', app_id, sequence_no, client_order_id, order_id, 
                                         cancel_reject_reason, transact_time, reserved)
        try:
            self.server.send_message(message)
        except: 
            self.server.request.close()  
        
        
        
        