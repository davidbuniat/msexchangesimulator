"""
@author: GarSing Chow
A simple admin client for the stock exchange.
"""
import socket
import struct
import time
from lse_protocol_decode_client import Decode 
from lse_protocol_encode_client import Encode 

def main():
    """
    The host and port of the server is set and a socket made.
    The client then connects to the server and logs in.
    It then checks the logon reply.
    Following this it continually recieves and processes data through the encoder and parser.
    """
    HOST, PORT = "localhost", 9999
    
    encoder = Encode()
    parser = Decode()
    
    try:

        # Create a socket (SOCK_STREAM means a TCP socket)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
        print "Connected to server."
        sock.sendall(encoder.logon())
        header = sock.recv(4)
        mesg_length = parser.read_header(header)
        if mesg_length > 0:
            message = parser.set_message(sock.recv(mesg_length))
        else:
            parser.set_message('')
        fail_reason = ''
        if message != struct.pack('<i30s', 0, '0'):
            fail_reason = fail_reason + 'LOGON FAILED'
            
        while 1:
            sock.sendall(encoder.make_message())
            header = sock.recv(4)
            mesg_length = parser.read_header(header)
            if mesg_length > 0:
                message = parser.set_message(sock.recv(mesg_length))
            else:
                parser.set_message('')
            parser.parse_message()
            time.sleep(1)
    
    finally:
        sock.close()
        
if __name__ == "__main__":
    main()


