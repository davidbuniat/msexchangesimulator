"""
@author: GarSing Chow
This module contains the readable client message parser for the native London Stock Exchange (Millenium) protocol. For more information on the protocol specification please see the LSE document MIT203 - Native Trading Gateway.
"""
__docformat__ = 'epytext'
import struct

class Decode:
    
    """
    This class is the decoder class that contains all the functions that handle message parsing from the procotol into the necessary function calls and variables.
    @type self.mesg_type: char
    @param self.mesg_type: The message type of the message.
    """ 
    def __logon_reply(self):
        """
        Decodes a logon reply message.
        """
        reject_code, passwd_expiry = struct.unpack('<i30s', self.message)
        return reject_code
        
    def read_header(self, header):
        """
        Takes a header of a message, unpacks the struct and returns the message length.
        @type header: string
        @param header: The header of a message.
        @rtype: int
        @return: The message length.
        """
        (mesg_start, mesg_length,
        self.mesg_type) = struct.unpack('<bhc', header)
        return mesg_length

    def set_message(self, message):
        """
        Sets the message of the class in order to be processed.
        @type self.message: string
        @param self.message: The message from the server.
        @rtype: string
        @return: The message.
        """
        self.message = message
        return message

    def __heartbeat(self):
        """
        Internal function that prints out that a heartbeat has been recieved.
        """
        print "This is a heartbeat."


    def __execution_report(self):
        """
        Decodes a execution report message, prints relevant information, then returns the decoded information.
        @rtype: dictionary
        @return: All the values within the execution report.
        
        """
        exec_rep = {}
        (exec_rep['app_id'], exec_rep['sequence_no'], exec_rep['execution_id'], exec_rep['client_order_id'], exec_rep['order_id'],
        exec_rep['exec_type'], exec_rep['execution_report_ref_id'], exec_rep['order_status'], exec_rep['order_reject_code'], 
        exec_rep['executed_price'], exec_rep['executed_qty'], exec_rep['leaves_qty'], exec_rep['container'], exec_rep['display_qty'], 
        exec_rep['instrument_id'], exec_rep['restatement_reason'], exec_rep['reserved1'], exec_rep['side'], exec_rep['reserved2'], 
        exec_rep['counterparty'], exec_rep['trade_liquidity_indicator'], exec_rep['trade_match_id'], exec_rep['transact_time'], 
        exec_rep['reserved3'], exec_rep['type_of_trade'], exec_rep['capacity'], exec_rep['price_differential'], 
        exec_rep['public_order_id']) = struct.unpack('<Bi12s20s12sc12sBiqiiBiiBbbQ11scQQ\
                                                           1sBBc12s', self.message)
        
        print('\nEXECUTION REPORT:\n''side: ' + str(exec_rep['side']) + ' ord_status: ' + str(exec_rep['order_status']) + ' exec_type: ' + exec_rep['exec_type'] + ' leaves_qty: ' + str(exec_rep['leaves_qty']) + ' executed_qty: ' + str(exec_rep['executed_qty']) + ' executed_price: ' + str(exec_rep['executed_price']) + '\n')
        return exec_rep

    def __order_cancel_reject(self):
        """
        Decodes a order cancel reject message, prints relevant information, then returns the order id of the rejected order.
        @rtype: string
        @return: The order id.
        """
        cancel_rej = {}
        (app_id, sequence_no, cancel_rej['client_order_id'], cancel_rej['order_id'], 
        cancel_reject_reason, transact_time, 
        reserved) = struct.unpack('<Bi20s12siQ10s', self.message)
        print('\nCANCEL REJECT:\nReason:' + str(cancel_reject_reason) + '\n')
        return cancel_rej

        
    def __success(self):
        """
        Prints a message showing a commands success.
        """
        print('Command successful!\n')
        
    def __fail(self):
        """
        Prints an error message.
        """
        print('Error processing command.\n')

    def parse_message(self):
        """
        Function that checks the message type contained within the class, and calls the correct function to process the message, otherwise printing an error message.
        """
        if self.mesg_type == 'B':
            return self.__logon_reply()
        elif self.mesg_type == '5':
            return self.__logout()
        elif self.mesg_type == '0':
            return self.__heartbeat()
        elif self.mesg_type == 'D':
            return self.__new_order()
        elif self.mesg_type == '8':
            return self.__execution_report()
        elif self.mesg_type == '9':
            return self.__order_cancel_reject()
        elif self.mesg_type == 'T':
            return self.__success()
        elif self.mesg_type == 't':
            return self.__fail()
        else: 
            print "This message type is unsupported"