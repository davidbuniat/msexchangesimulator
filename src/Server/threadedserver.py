"""
@author: GarSing Chow
@author: Neema Kotonya
This is the threaded server for the Stock Exchange.
"""

import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))[:-11])

import SocketServer
import threading
from src.Book.book import Book
from lse_protocol_decode import Decode
from lse_protocol_encode import Encode
from src.matching_engine import MatchingEngine



class MyThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    """
    The class overrides the default TCP server class.
    """
    def __init__(self, server_address, RequestHandlerClass, ME):
        """
        Initialises the server in order to use a single matching engine, and to allow the socket address to be reused as soon as the server shutsdown.
        @type server_address: tuple
        @param server_address: The host and port for the server.
        @type RequestHandlerClass: class
        @param RequestHandlerClass: The class for the request handler.
        """
        self.ME = ME
        self.allow_reuse_address = True
        self.daemon_threads = True
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)


class MyThreadedTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.
    It is instantiated once per connection to the server.
    """
    
    def send_message(self, message):
        """
        Sends a message to the client.
        @type message: string
        @param message: The message to be sent.
        """
        self.request.sendall(message)

    def logon(self, encoder, parser):
        """
        Processes a logon message from the client. If a logon message is not recieved or the logon is incorrect, the server is shutdown.
        @type encoder: Encode object
        @param encoder: The Encoder object for this client.
        @type parser: Decode object
        @param parser: The Decode object for this client.
        """
        header = self.request.recv(4)
        mesg_length = parser.read_header(header)
        if mesg_length != 76:
            encoder.logon_reply(107)
            self.server.close()
            return
        else:
            parser.set_message(self.request.recv(mesg_length))
            if parser.parse_message() == 0:
                self.server.close()
                return
            
    def recieve_data(self, encoder, parser):
        """
        Processes data from the client in a loop unless a logout is recieved. 
        First it recieves the data header, then parses the header through the parser. Using the message length from the header, it recieves the rest of the message and calls the parser to process the message.
        @type encoder: Encode object
        @param encoder: The Encoder object for this client.
        @type parser: Decode object
        @param parser: The Decode object for this client.
        """
        while 1:
            try:
                header = self.request.recv(4)
                while len(header) != 4:
                    header = self.request.recv(4)
            except:
                break
            
            mesg_length = parser.read_header(header)
            if mesg_length > 0:
                parser.set_message(self.request.recv(mesg_length))
            else:
                parser.set_message("")
            if parser.parse_message() == 1:
                break
        
    def handle(self):
        """
        The handler for the server. It creates the encoder and parser for the client, checks the logon, and procedes to recieve data from the client.
        """
        encoder = Encode(self)
        parser = Decode(self, self.server.ME, encoder)
        
        self.logon(encoder, parser)
        self.recieve_data(encoder, parser)
        self.request.close()
            

def main():
    """
    Main server setup. Sets the host and port, creates the matching engine. Creates and adds a book to the engine, and then starts the server.
    """
    HOST, PORT = "localhost", 9999

    ME = MatchingEngine()
    UCLBook = Book(123, 'UCL')
    ME.add_book(UCLBook)
    server = MyThreadedTCPServer((HOST, PORT), MyThreadedTCPHandler, ME)
    
    try:
        print "Main Stock Exchange server running. Stop server with Ctrl+C"
        server.serve_forever()
    except KeyboardInterrupt:
        sys.exit(0)    

if __name__ == "__main__":
    main()
