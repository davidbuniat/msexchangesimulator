'''
Created on 27 Nov 2014

@authors: Davit Buniatyan
@organization: UCL
'''

__docformat__ = 'epytext'
from src.Book.order import Order
import threading
import string
import random

class Book(object):
    """
    Book Class mainly contains two arrays of objects buy and sell order by their price
    @type self.lock: RLock
    @param self.lock: locking the book object for avoiding concurrency bugs
    @type self.instrument_ID: Integer
    @param self.instrument_ID: The ID of the instrument
    @type self.security: String
    @param self.security: The name of the instrument (Book)
    @type self.buy_list: list
    @param self.buy_list: Order list of buyer's side sorted by their price
    @type self.sell_list: list
    @param self.sell_list: Order list of selling's side sorted by their price
    @type self.expired_list: list
    @param self.expired_list: expired orders by their price
    """
   
    def __init__(self, instrument_ID, security=''):
        self.lock = threading.RLock()
        self.instrument_ID = instrument_ID
        self.security = security
        self.buy_list = []
        self.sell_list = []
        self.expired_list = {}
    
    def add_order(self, order):
        """
        Add order to the list and avoid duplication of the id for each book
        @type order: Order
        @param order: the object that should be added
        """
        #Avoid Duplication of ID for each Book
        if not ((self.get_order_by_id(order.getID()['OrderID'], 'sell')==None) & (self.get_order_by_id(order.getID()['OrderID'], 'buy')==None)):
            randomID = ''.join(random.choice(string.ascii_uppercase) for i in range(12))
            order.getID()['OrderID'] = randomID
            self.add_order(order)
            return
        
        with self.lock:
            if order.getOrder()['side'] == 'buy':
                self.add_sorted(self.buy_list, order )
            elif order.getOrder()['side'] == 'sell':
                self.add_sorted(self.sell_list, order )
   
    def add_sorted(self, list, order): 
        """
        Puts the element in the sorted place
        @type list: List
        @param list: The list that the order should be added
        @type order: Order
        @param order: the object that should be added
        """
        with self.lock:   
            if len(list)==0:
                list.append(order)
            else:
                if order.getOrder()['side'] == 'sell':
                    for i in xrange(0,len(list)):
                        if order.getOrder()['price'] <= list[i].ord['price']:
                            list.insert(i, order)
                            break
                
                    if order.getOrder()['price'] > list[len(list)-1].ord['price']:
                        list.insert(len(list),order )
                    
                elif order.getOrder()['side'] == 'buy':
                    for i in xrange(0,len(list)):
                        if order.getOrder()['price'] >= list[i].ord['price']:
                            list.insert(i, order)
                            break
                    
                    if order.getOrder()['price'] < list[len(list)-1].ord['price']:
                        list.insert(len(list),order)

                    
    def get_buy(self):
        """
        Returns the buy list
        @rtype: List
        @return: Buying list of orders
        """
        
        with self.lock: 
            return self.buy_list
    
    def get_sell(self):
        """
        Returns the sell list
        @rtype: List
        @return: Selling list of orders
        """
        with self.lock: 
            return self.sell_list
    
    def get_list(self, side):
        """
        Returns the list of specified Side
        @type side: String
        @param side: the side of the list
        @rtype: List
        @return: List of orders
        """
        with self.lock: 
            if side == 'buy':
                return self.get_buy()
            elif side == 'sell':
                return self.get_sell()
        
    def get_list_not(self, side):
        """
        Returns the other side list
        @type side: String
        @param side: the side of the list
        @rtype: List
        @return:  List of orders
        """
        with self.lock: 
            if side == 'buy':
                return self.get_sell()
            elif side == 'sell':
                return self.get_buy()
            
    def lock_book(self):
        """
        Look the book for other threads
        """
        self.lock.acquire()
        
    def unlock_book(self):
        """
        Unlock the book from the same thread
        """
        self.lock.release()
        
    def get_expired_list(self):
        """
        Returns the other expired order list
        @rtype: List
        @return:  List of expired orders
        """
        with self.lock:
            return self.expired_list
        
    def remove_order(self, order):
        """
        Returns the other side list
        @type order: Order
        @param order: the side of the list
        @rtype: Boolean
        @return: if succeeded to remove then return true otherwise false 
        """
        with self.lock:
            if order.getOrder()['side'] == 'buy':
                try:
                    self.get_expired_list()[order.getID()['OrderID']] = order #Move to expired
                    self.get_buy().remove(order)
                    
                except ValueError:
                    return False
                return True
            elif order.getOrder()['side'] == 'sell':
                try:
                    self.get_expired_list()[order.getID()['OrderID']] = order #Move to expired
                    self.get_sell().remove(order)  
                except ValueError:
                    return False
                return True

    def is_expired(self,order):
        """
        Check if the order is expired
        @type order: Order
        @param order: ID of the order
        @rtype: Boolean
        @return: if the order is in the list of expired order list
        """
        return self.expired_list.has_key(order.getID()['OrderID'])
    
    def get_order_by_id(self, order_id, side):
        """
        Returns the other order by its id and side
        @type order_id: String
        @param order_id: ID of the order
        @type side: String
        @param side: the side of the order
        @rtype: Order
        @return:  the order found in the list
        """
        with self.lock:
            # Look in Sell or Buy lists
            list = self.get_list(side)
            
            for i in xrange(0,len(list)):
                if list[i].getID()['OrderID'] == order_id:
                    order = list[i]
                    return order
                
            # Look in expired list
            list = self.expired_list
            try:
                order = self.get_expired_list()[order_id]
            except KeyError:
                order = None 
                
            return order
        
    def get_order_by_client_order_id(self, client_order_id, side):
        """
        Returns the other order by its id set by client and side
        @type client_order_id: String
        @param client_order_id: Client ID of the order
        @type side: String
        @param side: the side of the order
        @rtype: Order
        @return:  the order found in the list
        """
        with self.lock:
            # Look in Sell or Buy lists
            list = self.get_list(side)
            
            for i in xrange(0,len(list)):
                if list[i].getID()['ClientOrderID'] == client_order_id:
                    order = list[i]
                    return order
                
            # Look in expired list
            list = self.expired_list
            order = None
            for orderID in iter(self.get_expired_list()):
                if self.get_expired_list()[orderID].getID()['ClientOrderID'] == client_order_id:
                    order = self.get_expired_list()[orderID]
                    break
                
            return order
                    
                

 
                