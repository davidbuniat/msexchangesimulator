from multiprocessing.connection import Client #Not really sure what this is for.

from src.Server.lse_protocol_encode import Encode
import string
import random

"""
@authors: Davit Buniatyan, GarSing Chow
@organization: UCL
"""
__docformat__ = 'epytext'
class Order: 
    """
    Order class epresent the core object which contains three dictionaries id, ord and sts.
    @type self.id: Dictionary
    @param self.id: ID dictionary
    @type self.ord: Dictionary
    @param self.ord: Order information
    @type self.sts: Dictionary
    @param self.sts: Order State
    @type self.idencoderID: Encoder
    @param self.encoderID: Encoder Object
    """
    def __init__(self):
        """
        Sets the ID, initiate dictionaries id, ord and sts, the encoder and the instrument ID
        """
        self.id = {}
        self.ord = {}
        self.sts = {}
        self.id['OrderID'] = ''.join(random.choice(string.ascii_uppercase) for i in range(12)) #Set The id by random number
        self.encoderID = Encode()
        self.id['InstrumentID'] = 00001

    def setID(self, client_order_id, trader_id, instrument_id, user_id=''):

        """
        Sets Identification of the order   
        @type client_order_id: String   
        @param client_order_id: The Order ID set by Client
        @type trader_id: String
        @param trader_id: The ID of Trader (NOT USED)
        @type instrument_id: Integer
        @param instrument_id: Instrument ID (Book ID) Field used by LSE instead of security
        @type user_id: String
        @param user_id: User's ID
        
        """
        self.id['ClientOrderID'] = client_order_id
        self.id['TraderID'] = trader_id 
        self.id['InstrumentID'] = instrument_id
        self.id['UserID'] = user_id
    
    def getID(self):
        """
        Returns Dictionary Object of ID
        @rtype: Dictionary      
        @return: Object Dictionary of All IDs
        """
        return self.id
        
    def setOrder(self, security, order_price, order_qty, side, order_type, tif = "day"):
        """
        Set's Order information 
        @type security: String
        @param security: The name of security
        @type order_price: Integer
        @param order_price: The price of the order
        @type order_qty: Integer
        @param order_qty: The quantity of the order
        @type side: String
        @param side: 'sell' or 'buy' side of the order
        @type order_type: String
        @param order_type: 'market' or 'limit'
        @type tif: String
        @param tif: 'day' (Default Value) or 'foc' 'ioc'
        """
        self.ord['security'] = security
        self.ord['price'] = order_price
        self.ord['quantity'] = order_qty
        self.ord['side'] = side
        self.ord['type'] = order_type
        self.ord['tif'] = tif
        
        self.setState("","",order_qty, 0, 0)
    
    def getOrder(self):
        """
        Returns Dictionary Order  
        @rtype: Dictionary
        @return: Order object's information
        """
        return self.ord
        
    def setState(self, ord_Status, exec_type, leaves_qty, executed_qty, executed_price):
        """
        Set's Order's State
        @param ord_Status: Order Status 0 New, 1 Partially, 2 Filled, 4 Cancelled, 6 Expired Filled, 8 Rejected, 9 Suspended
        @type ord_Status: String
        @param exec_type: Execution Type 0 New, 4 Cancelled, 5 Replaced, 8 Rejected, C Expired, D Restated, F Trade, G Trade Correct, H Trade Cancel, 9 Suspended 
        @type exec_type: Integer
        @param leaves_qty: The quantity of the order
        @type leaves_qty: Integer
        @param executed_qty: 'sell' or 'buy' side of the order
        @type executed_qty: String
        @param executed_price: 'market' or 'limit'
        @type executed_price: String
        """
        self.sts['order'] = ord_Status #0 New, 1 Partially, 2 Filled, 4 Cancelled, 6 Expired Filled, 8 Rejected, 9 Suspended
        self.sts['exec_type'] = exec_type #0 New, 4 Cancelled, 5 Replaced, 8 Rejected, C Expired, D Restated, F Trade, G Trade Correct, H Trade Cancel, 9 Suspended 
        self.sts['leaves_qty']= leaves_qty
        self.sts['executed_qty'] = executed_qty
        self.sts['executed_price'] = executed_price
    
    def getState(self):
        """
        Returns Dictionary Order
        @rtype: Dictionary
        @return: Order object's information
        """
        return self.sts
      
    def newOrder(self, client_order_id, trader_id, instrument_id, order_type, TIF, side, order_qty, limit_price, security):
        """
        Creating an order and translating into matching engine format 
        @param client_order_id: The id set by client
        @type client_order_id: String
        @param trader_id: Traders ID (Not Used)
        @type trader_id: String
        @param instrument_id: Instruments ID
        @type instrument_id: Integer
        @param order_type: The type of the order 'market' or 'limit'
        @type order_type: String
        @param TIF: (Default 'Day') ioc or fok
        @type TIF: String
        @param side: 'sell' or 'buy'
        @type side: String
        
        @param order_qty: The quantity of the order
        @type order_qty: Integer
        @param limit_price: the limit price
        @type limit_price: Integer
        @param security: Security name
        @type security: String
        """
        if side == 1:
            side = 'buy'
        elif side == 2:
            side = 'sell'
            
        if order_type == 1:
            order_type = 'limit'
        elif order_type == 2:
            order_type = 'market'
            
        if TIF == 0:
            TIF = 'day'
        elif TIF == 4:
            TIF = 'fok'
        elif TIF == 3:
            TIF = 'ioc'
        
        self.setID(client_order_id, trader_id, instrument_id, 0)
        self.setOrder(security, limit_price, order_qty, side, order_type, TIF)
        
    def setEncoderID(self, encoderID):
        """Sets encoder"""
        self.encoderID = encoderID
        
    def setOrderType(self, orderType):
        """Sets order type"""
        self.ord['type'] = orderType
