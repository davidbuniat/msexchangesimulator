'''
Created on 30 Nov 2014

@author: Davit Buniatyan
@organization: UCL
'''

__docformat__ = 'epytext'
from src.Book.book import Book
from src.Book.order import Order
class Report(object):
    """
    Report Module reports the status changes of the order to the client and is called from Matching Engine.
    """
    def __init__(self):
        return
        
    def min(self, order, ord_status, exec_type):
        """
        Fully reports the order. (Not Used)
        @type order: Order
        @param order: The object that is going to be reported
        @type ord_status: String
        @param ord_status: Order Status
        @type exec_type: String
        @param exec_type: The execution type
        """
        self.all(order)
        
    def all(self, order):
        """
        Fully reports the execution and the state of the order.
        @type order: Order
        @param order: The order that is going to be reported
        """
        ord_status = order.getState()['order']
        exec_type  = order.getState()['exec_type']
        leaves_qty = order.getState()['leaves_qty']
        executed_qty = order.getState()['executed_qty']
        executed_price = order.getState()['executed_price']
        
        #print('id: ' + order.getID()['OrderID'] + ' side: ' + str(order.ord['side']) + ' ord_status: ' + ord_status + ' exec_type: ' + exec_type + ' leaves_qty: ' + str(leaves_qty) + ' executed_qty: ' + str(executed_qty) + ' executed_price: ' + str(executed_price))
        try:
            order.encoderID.send_execution_report(order.ord['side'], ord_status, exec_type, leaves_qty, executed_qty, executed_price, order.getID()['OrderID'], order.getID()['ClientOrderID'])
        except:
            print('An error has occurred sending an execution report.')
        return
    
    def cancel(self, order, reason):
        """
        Reject the cancel or the order with the given reason
        @type order: Order
        @param order: the order that was rejected.
        @type reason: OrdSter
        @param reason: the order that was rejected.
        """
        try:
            order.encoderID.send_order_cancel_reject(order.getID()['ClientOrderID'],  order.getID()['OrderID'], reason)
        except:
            print('An error has occurred sending a cancel reject message.')
        return
        
    def print_book_info(self, book):
        """
        Print the book info
        @type book: Book
        @param book: The book that should be reported
        """
        book.lock_book()
        buy = book.get_list('buy')
        sell = book.get_list('sell')
        exp = book.get_expired_list()

        print('Book: ' + str(book.instrument_ID)  + " " + book.security)
        print('----------------------------------------------------------------------')
        print('Buy Side: ')
        for item in buy:
            buyOrder = " id: " + item.getID()['OrderID'] + " quantity: " + str(item.getState()["leaves_qty"]) + "\t price: " + str(item.getOrder()["price"]) 
            print(buyOrder)
        print('Sell Side: ')
        for item in sell:
            sellOrder = " id: " + item.getID()['OrderID'] + " quantity: "  + str(item.getState()["leaves_qty"]) + "\t price: " + str(item.getOrder()["price"]) 
            print(sellOrder)
        print('Expired Orders: ')
        for item in iter(exp):
            expOrder = " id: " + exp[item].getID()['OrderID'] + " quantity: "  + str(exp[item].getState()["leaves_qty"]) + "\t price: " + str(exp[item].getOrder()["price"]) 
            print(expOrder)

        print('----------------------------------------------------------------------')
        book.unlock_book()
        
    def print_list_of_books(self, books):
        """
        Print all books info
        @type books: Dictionary
        @param books: The books list that should be reported
        """
        print('All Books:')
        print('----------------------------------------------------------------------')
        for book in iter(books):
            print('InstrumentID: ' + str(books[book].instrument_ID)  + "\t  Security " + books[book].security)
        print('----------------------------------------------------------------------')
        
    def print_order(self, o):
        """
        Print the order
        @type o: Order
        @param o: The Order that should be printed
        """
        print('Order:')
        print('----------------------------------------------------------------------')
        print('OrderID: ' + o.getID()['OrderID'])
        print('InstrumentID: ' + str(o.getID()['InstrumentID']))
        print('ClientOrderID: ' + str(o.getID()['ClientOrderID']))
        print('UserID: ' + str(o.getID()['UserID']))
        print(' ')
        print('Security: ' + o.getOrder()['security'])
        print('Price: ' + str(o.getOrder()['price']))
        print('Quantity: ' + str(o.getOrder()['quantity']))
        print('Side: ' + o.getOrder()['side'])
        print('Type: ' + o.getOrder()['type'])
        print(' ')
        print('OrderStatus: ' + o.getState()['order'])
        print('Execution type: ' + o.getState()['exec_type'])
        print('Left Quantity: ' + str(o.getState()['leaves_qty']))
        print('Executed Quantity: ' + str(o.getState()['executed_qty']))
        print('Executed Price: ' + str(o.getState()['executed_qty']))
        print('----------------------------------------------------------------------')
        
