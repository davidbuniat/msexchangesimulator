"""
@author: GarSing Chow
This module contains the test data classes for the test clients.
"""
import struct

class new_order():
    """
    This class creates a new order object that contains the message and variables for a new order.
    """

    def __init__(self, client_order_id, order_type, tif, side, order_qty, display_qty, limit_price):
        """
        Initialises the values for the new order object. and creates the packed message from the data.
        @type client_order_id: string
        @param client_order_id: The client order id for the order.
        @type order_type: int
        @param order_type: The order type of this order.
        @type tif: int
        @param tif: the TIF of this order.
        @type side: int
        @param side: the side of this order.
        @type order_qty: int
        @param order_qty: The quantity of shares in this order.
        @type display_qty: int
        @param display_qty: The display quantity of this order.
        @type limit_price: int
        @param limit_price: The limit price of the order.
        """
        
        self.header = struct.pack('<bhc', 2, 93, 'D')
        client_order_id = client_order_id
        trader_id = 'trader_id'
        account = 'account'
        clearing_account = 1
        instrument_id = 123
        reserved1 = 0
        reserved2 = 0
        order_type = order_type
        tif = tif
        expiry_date_time = 0
        side = side
        order_qty = order_qty
        display_qty = display_qty
        limit_price = limit_price
        capacity = 1
        auto_cancel = 0
        order_sub_type = 0
        anonymity = 0
        stopped_price = 0
        passive_only_order = 0
        reserved3 = ''
        
        self.message = struct.pack('<20s11s10sBibbBBIBiiqBBBBqB9s', client_order_id,
                                    trader_id, account, clearing_account, instrument_id,
                                    reserved1, reserved2, order_type, tif, expiry_date_time, side,
                                    order_qty, display_qty, limit_price, capacity, auto_cancel,
                                    order_sub_type, anonymity, stopped_price, passive_only_order,
                                    reserved3)
        
class new_execution_report():
    """
    This class creates a new execution report object that contains the message and variables for a new execution report.
    """
    
    def __init__(self, client_order_id, order_id, side, order_status, exec_type, leaves_qty, executed_qty, executed_price):
        """
        Initialises the values for the new execution report object. and creates the packed message from the data.
        @type client_order_id: string
        @param client_order_id: The order id for the order.
        @type order_id: string
        @param order_id: The order id for the order.
        @type side: int
        @param side: the side of the order.
        @type order_status: int
        @param order_status: The order type of the order.
        @type exec_type: char
        @param exec_type: The execution type of the order.
        @type leaves_qty: int
        @param leaves_qty: The leaves quantity of shares in this order.
        @type executed_qty: int
        @param executed_qty: The executed quantity of this order.
        @type executed_price: int
        @param executed_price: The executed price of the order.
        """
        
        self.header = struct.pack('<bhc', 2, 147, '8')
        app_id = 1
        sequence_no = 1
        execution_id = 'undef'
        execution_report_ref_id = 'undef'
        order_reject_code = 1 
        container = 1
        display_qty = 1
        instrument_id = 1
        restatement_reason = 1
        reserved1 = 0
        reserved2 = 0 
        counterparty = 'undef'
        trade_liquidity_indicator = 'A'
        trade_match_id = 1
        transact_time = 0 
        reserved3 = '0'
        type_of_trade = 2
        capacity = 0
        price_differential = 'A' 
        public_order_id = 'undef'
        side = side
        order_status = order_status
        exec_type = exec_type
        leaves_qty = leaves_qty
        executed_qty = executed_qty
        executed_price = executed_price
        self.message = struct.pack('<Bi12s20s12sc12sBiqiiBiiBbbQ11scQQ1sBBc12s', app_id, sequence_no, execution_id, client_order_id, order_id,
                                    exec_type, execution_report_ref_id, order_status, order_reject_code, 
                                    executed_price, executed_qty, leaves_qty, container, display_qty, 
                                    instrument_id, restatement_reason, reserved1, side, reserved2, 
                                    counterparty, trade_liquidity_indicator, trade_match_id, transact_time, 
                                    reserved3, type_of_trade, capacity, price_differential, 
                                    public_order_id)
        
class new_order_cancel_request():
    """
    This class creates a new order cancel request object that contains the message and variables for a new order cancel request report.
    """
    def __init__(self, client_order_id, original_client_order_id, order_id, instrument_id, side):
        """
        Initialises the values for the new order cancel request object. and creates the packed message from the data.
        @type client_order_id: string
        @param client_order_id: The client order id for the order.
        @type original_client_order_id: string
        @param original_client_order_id: The original client order id for the order.
        @type order_id: string
        @param order_id: The order id for the order.
        @type instrument_id: int
        @param instrument_id: The intrument id of this order.
        @type side: int
        @param side: the side of the order.
        """
        
        self.header = struct.pack('<bhc', 2, 69, 'F')
        client_order_id = client_order_id
        original_client_order_id = original_client_order_id
        instrument_id = instrument_id
        reserved1 = 0
        reserved2 = 0
        side = side
        reserved3 = ''
        self.message = struct.pack('<20s20s12sibbB10s', client_order_id, original_client_order_id, order_id, 
                                    instrument_id, reserved1, reserved2, side, reserved3)
        
class new_order_cancel_replace_request():
    """
    This class creates a new order cancel replace request object that contains the message and variables for a new order cancel request report.
    """
    
    def __init__(self, client_order_id, original_client_order_id, order_id, instrument_id, order_qty, display_qty, limit_price, tif, side):
        """
        Initialises the values for the new order cancel replace request object. and creates the packed message from the data.
        @type client_order_id: string
        @param client_order_id: The client order id for the order.
        @type original_client_order_id: string
        @param original_client_order_id: The original client order id for the order.
        @type order_id: string
        @param order_id: The order id for the order.
        @type instrument_id: int
        @param instrument_id: The intrument id of this order.
        @type tif: int
        @param tif: the TIF of this order.
        @type side: int
        @param side: the side of this order.
        @type order_qty: int
        @param order_qty: The quantity of shares in this order.
        @type display_qty: int
        @param display_qty: The display quantity of this order.
        @type limit_price: int
        @param limit_price: The limit price of the order.
        """
        
        self.header = struct.pack('<bhc', 2, 118, 'G')
        reserved1 = 0
        reserved2 = 0
        expire_date_time = 0
        account = 'account'
        tif = tif
        side = side
        stopped_price = -1
        passive_only_order = 0
        reserved3 = ''
        self.message = struct.pack('<20s20s12sibbIiiq10sbbqb9s', client_order_id, original_client_order_id, 
                                    order_id, instrument_id, reserved1, reserved2, expire_date_time, order_qty, 
                                    display_qty, limit_price, account, tif, side, stopped_price, passive_only_order,
                                    reserved3)
        
class new_order_cancel_reject():
    """
    This class creates a new order cancel reject object that contains the message and variables for a new order cancel reject.
    """
    def __init__(self, client_order_id, order_id, cancel_reject_reason = 0):
        """
        Initialises the values for the new order cancel reject object. and creates the packed message from the data.
        @type client_order_id: string
        @param client_order_id: The order id set by Client
        @type order_id: string
        @param order_id: The order id for the order.
        @type cancel_reject_reason: int
        @param cancel_reject_reason: The cancel reject reason for the order.
        """
        self.header = struct.pack('<bhc', 2, 59, '9')
        app_id = 1
        sequence_no = 1
        transact_time = 1
        reserved = ''
        self.message = struct.pack('<Bi20s12siQ10s', app_id, sequence_no, client_order_id, order_id, 
                                         cancel_reject_reason, transact_time, reserved)
        