'''
Created on 29 Nov 2014

@author: dav
'''
import unittest
from src.Book.book import Book
from src.Book.order import Order
from src.matching_engine import MatchingEngine
import thread

import time

 



class Test(unittest.TestCase):
   
    def test_add_book_0(self):
        ME = MatchingEngine()
        UCLBook = Book(000001,'UCL')
        ME.add_book(UCLBook)
        
        assert UCLBook == ME.get_book(000001)
    
    """Testing can_be_matched()"""
    def test_can_be_matched_0(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'sell', 'market') 
        
        assert ME.can_be_matched(order1, order2) == True
   
    def test_can_be_matched_01(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 50, 200, 'buy', 'market') 
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'sell', 'market') 
        
        assert ME.can_be_matched(order1, order2) == True
    
    def test_can_be_matched_02(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 30, 200, 'buy', 'market') 
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'sell', 'market') 
        
        assert ME.can_be_matched(order1, order2) == False
        
    def test_can_be_matched_03(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 50, 200, 'sell', 'market') 
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        
        assert ME.can_be_matched(order1, order2) == False
    
    def test_can_be_matched_05(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 40, 200, 'sell', 'market') 
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        
        assert ME.can_be_matched(order1, order2) == True
    
    def test_can_be_matched_06(self):
        ME = MatchingEngine()
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 30, 200, 'sell', 'market') 
        order2 = Order()
        order1.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        
        assert ME.can_be_matched(order1, order2) == True
    
    """Limit Type Order Matching"""
    def test_process_order_00(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 40, 200, 'buy', 'limit') 
        
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 150, 'sell', 'limit') 
        
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)
        
    def test_process_order_0(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')


        order1 = Order()
        order1.setOrder( 'UCL', 40, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 200, 'sell', 'limit') 
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 0)
        
    def test_process_order_01(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 50, 200, 'buy', 'limit')
         
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 200, 'sell', 'limit') 
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell())) == 0 & (len(ME.get_book(000001).get_buy()) == 0)
        
    def test_process_order_02(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 30, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 200, 'sell', 'limit') 
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell()) == 1) & (len(ME.get_book(000001).get_buy()) == 1)
        
    def test_process_order_03(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        #('test_Limit_3','','','')
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 50, 100, 'buy', 'limit') 
        order2 = Order()
        #('test_Limit_3','','','')
        order2.setOrder( 'UCL', 40, 200, 'sell', 'limit')
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)

        assert (len(ME.get_book(000001).get_sell()) == 1) & (len(ME.get_book(000001).get_buy()) == 0)
        
    def test_process_order_04(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 50, 200, 'buy', 'limit') 
        #('test_Limit_4','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 100, 'sell', 'limit') 
        #('test_Limit_4','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)
      
    def test_process_order_05(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 5, 200, 'sell', 'limit') 
        #('test_Limit_5','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 4, 100, 'sell', 'limit') 
        #('test_Limit_5','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 4, 150, 'buy', 'limit') 
        #('test_Limit_5','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        
        assert (len(ME.get_book(000001).get_sell()) == 1) & (len(ME.get_book(000001).get_buy()) == 1)
    
    def test_process_order_06(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 50, 200, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 40, 100, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 60, 350, 'buy', 'limit') 
        #('test_Limit_6','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)
        
    def test_process_order_07(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 50, 200, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 50, 350, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 60, 100, 'buy', 'limit') 
        #('test_Limit_6','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        
        assert (len(ME.get_book(000001).get_sell()) == 1) & (len(ME.get_book(000001).get_buy()) == 0)
        
    def test_process_order_08(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 4, 50, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 5, 50, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 6, 50, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order4 = Order()
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        order4.setOrder( 'UCL', 5, 40, 'sell', 'limit') 
        #('test_Limit_6','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 3)
        
    def test_process_order_09(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 4, 50, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 5, 50, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 6, 50, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order4 = Order()
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        order4.setOrder( 'UCL', 7, 160, 'buy', 'limit') 
        #('test_Limit_6','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)
        
    def test_process_order_10(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order1.setOrder( 'UCL', 4, 50, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order2 = Order()
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order2.setOrder( 'UCL', 5, 60, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order3 = Order()
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order3.setOrder( 'UCL', 6, 70, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order4 = Order()
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        order4.setOrder( 'UCL', 5, 80, 'sell', 'limit') 
        #('test_Limit_6','','','')
        order5 = Order()
        order5.setID("client_order_id_5", "trader_id", 000001, "order_id")
        order5.setOrder( 'UCL', 4, 40, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order6 = Order()
        order6.setID("client_order_id_6", "trader_id", 000001, "order_id")
        order6.setOrder( 'UCL', 5, 90, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order7 = Order()
        order7.setID("client_order_id_7", "trader_id", 000001, "order_id")
        order7.setOrder( 'UCL', 6, 50, 'buy', 'limit') 
        #('test_Limit_6','','','')
        order8 = Order()
        order8.setID("client_order_id_8", "trader_id", 000001, "order_id")
        order8.setOrder( 'UCL', 5, 30, 'sell', 'limit') 
        #('test_Limit_6','','','')
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        ME.process_order(order5)
        ME.process_order(order6)
        ME.process_order(order7)
        ME.process_order(order8)

        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 4)


    """ Market OrderType Matching"""
    def test_process_order_11(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 40, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 40, 200, 'sell', 'market') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 0)
        
    def test_process_order_12(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        
        order4 = Order()
        order4.setOrder( 'UCL', 0, 350, 'sell', 'market') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 2)

    def test_process_order_13(self):
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'sell', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'sell', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'sell', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        
        order4 = Order()
        order4.setOrder( 'UCL', 0, 350, 'buy', 'market') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 2) & (len(ME.get_book(000001).get_buy()) == 0)
    
    def test_process_order_14(self):
        print("--- 14 ---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'sell', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'sell', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'sell', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        
        order4 = Order()
        order4.setOrder( 'UCL', 0, 650, 'buy', 'market') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        print("end --- 14 ---")
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)

    """ IOC OrderType Matching"""
    def test_process_order_15(self):
        print("---IoC test 15---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 5, 350, 'sell','limit','ioc') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 2)
        
    def test_process_order_16(self):
        print("---IoC test 16---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit')
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id") 
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 4, 450, 'sell','limit', 'ioc') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1) & (ME.get_book(000001).get_buy()[0].sts['leaves_qty'] == 150)
                                                                                                       
    def test_process_order_17(self):
        print("---IoC test 17---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 4, 650, 'sell', 'limmit', 'ioc')
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id") 
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 0)
   
    """ FOK OrderType Matching"""  
    def test_process_order_18(self):
        print("---FoK test 18---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit') 
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id")
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 4, 650, 'sell','limit',  'fok') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 3)
    
    def test_process_order_19(self):
        print("--- FoK test 19---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit')
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id") 
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 5, 450, 'sell','limit', 'fok') 
        order4.setID("client_order_id", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 3)
    
    def test_process_order_20(self):
        print("---FoK test 20---")
        ME = MatchingEngine()
        UCL = Book(000001,'UCL')
        
        
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit')
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id") 
        order2 = Order()
        order2.setOrder( 'UCL', 5, 200, 'buy', 'limit') 
        order2.setID("client_order_id_2", "trader_id", 000001, "order_id")
        order3 = Order()
        order3.setOrder( 'UCL', 6, 200, 'buy', 'limit') 
        order3.setID("client_order_id_3", "trader_id", 000001, "order_id")
        order4 = Order()
        order4.setOrder( 'UCL', 4, 550, 'sell','limit', 'fok') 
        order4.setID("client_order_id_4", "trader_id", 000001, "order_id")
        
        ME.add_book(UCL)
        ME.process_order(order1)
        ME.process_order(order2)
        ME.process_order(order3)
        ME.process_order(order4)
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 1)
    
    def test_fill_order_21(self):
        print("---Fill Order By id test 21---")
        ME = MatchingEngine()
        order1 = Order()
        order1.setOrder( 'UCL', 4, 200, 'buy', 'limit')
        order1.setID("client_order_id_1", "trader_id", 000001, "order_id") 
       
        ME.process_order(order1)
        time.sleep(0.1)
        ME.fill_order_only_by_id(order1.getID()['OrderID'])
        print("---Fill Order By id test 21---")
        assert (len(ME.get_book(000001).get_sell()) == 0) & (len(ME.get_book(000001).get_buy()) == 0)      
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()