'''
Created on 29 Nov 2014

@author: dav
'''
import unittest
from src.Book.book import Book
from src.Book.order import Order
from StdSuites.Type_Names_Suite import null

class Test(unittest.TestCase):

    #Unit Testing
    def test_add_order_0(self):
        UCLBook = Book('UCL')
        order1 = Order()
        order1.setOrder( 'UCL', 20, 200, 'buy', 'market') 
        UCLBook.add_order(order1)      
        assert UCLBook.get_buy()[0].ord['price'] == 20

    def test_add_order_1(self):
        UCLBook = Book('UCL')
        order1 = Order()
        order1.setOrder( 'UCL', 20, 200, 'buy', 'market') 
        order2 = Order()
        order2.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        order3 = Order()
        order3.setOrder( 'UCL', 60, 200, 'buy', 'market') 
        
        UCLBook.add_order(order1)
        UCLBook.add_order(order2)
        UCLBook.add_order(order3)
        assert UCLBook.get_buy()[0].ord['price']>UCLBook.get_buy()[1].ord['price'] & UCLBook.get_buy()[1].ord['price']>UCLBook.get_buy()[2].ord['price']

    def test_add_order_2(self):
        UCLBook = Book('UCL')
        order1 = Order()
        order1.setOrder( 'UCL', 30, 200, 'sell', 'market') 
        order2 = Order()
        order2.setOrder( 'UCL', 20, 200, 'sell', 'market') 

        UCLBook.add_order(order1)
        UCLBook.add_order(order2)
        assert UCLBook.get_sell()[0].ord['price'] < UCLBook.get_sell()[1].ord['price']

    def test_add_order_3(self):
        UCLBook = Book('UCL')
        order1 = Order()
        order1.setOrder( 'UCL', 20, 200, 'buy', 'market') 
        order2 = Order()
        order2.setOrder( 'UCL', 40, 200, 'buy', 'market') 
        order3 = Order()
        order3.setOrder( 'UCL', 60, 200, 'buy', 'market') 

        UCLBook.add_order(order1)
        UCLBook.add_order(order2)
        UCLBook.add_order(order3)
        assert (UCLBook.get_buy()[0].ord['price']>UCLBook.get_buy()[1].ord['price']) & (UCLBook.get_buy()[1].ord['price']>UCLBook.get_buy()[2].ord['price'])

    def test_add_order_4(self):
        UCLBook = Book('UCL')

        order1 = Order()
        order1.setOrder( 'UCL', 252, 200, 'buy', 'market') 
        order2 = Order()
        order2.setOrder( 'UCL', 100, 200, 'buy', 'market') 
        order3 = Order()
        order3.setOrder( 'UCL', 300, 200, 'buy', 'market') 
        UCLBook.add_order(order1)
        UCLBook.add_order(order2)
        UCLBook.add_order(order3)
        assert (UCLBook.get_buy()[0].ord['price']>UCLBook.get_buy()[1].ord['price']) & (UCLBook.get_buy()[1].ord['price']>UCLBook.get_buy()[2].ord['price']) & (UCLBook.get_buy()[1].ord['price']>UCLBook.get_buy()[2].ord['price'])

    def test_get_order_by_id_1(self):
        
        UCLBook = Book(000001,'UCL')

        order1 = Order()
        order1.setID("client_order_id", "trader_id", 000001, "01")
        order1.setOrder( 'UCL', 252, 200, 'buy', 'market') 

        UCLBook.add_order(order1)
        order = UCLBook.get_order_buy_id("01", 'buy')
        assert order!=null
    
    def test_get_order_by_id_2(self):
        
        UCLBook = Book(000001,'UCL')

        order1 = Order()
        order1.setID("client_order_id", "trader_id", 000001, "01")
        order1.setOrder( 'UCL', 252, 200, 'buy', 'market') 

        UCLBook.add_order(order1)
        order = UCLBook.get_order_buy_id("02", 'buy')
        assert order==None
   
    def test_get_order_by_id_3(self):
        
        UCLBook = Book(000001,'UCL')

        order1 = Order()
        order1.setID("client_order_id", "trader_id", 000001, "01")
        order1.setOrder( 'UCL', 252, 200, 'buy', 'market') 

        UCLBook.add_order(order1)
        order = UCLBook.get_order_buy_id("01", 'sell')
        assert order==None
    
    def test_get_order_by_id_4(self):
        
        UCLBook = Book(000001,'UCL')

        order1 = Order()
        order1.setID("client_order_id", "trader_id", 000001, "01")
        order1.setOrder( 'UCL', 252, 200, 'buy', 'market') 

        UCLBook.expired_list["01"] = order1
        order = UCLBook.get_order_buy_id("01", 'sell')
        assert order!=None

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()