from src.Book.book import Book
from src.Book.order import Order
from src.matching_engine import MatchingEngine

#Unit Testing
def test_add_order_0():
	UCLBook = Book('UCL')
	order1 = Order('UCL',20, 'buy', 100, 'smth', '0001')
	UCLBook.add_order(order1)
	assert UCLBook.get_buy()[0].price == 100

def test_add_order_1():
	UCLBook = Book('UCL')
	order1 = Order('UCL',20, 'buy', 100, 'smth', '0001')
	order2 = Order('UCL',20, 'buy', 200, 'smth', '0001')
	UCLBook.add_order(order1)
	UCLBook.add_order(order2)
	assert UCLBook.get_buy()[0].price<UCLBook.get_buy()[1].price

def test_add_order_2():
	UCLBook = Book('UCL')
	order1 = Order('UCL',20, 'buy', 200, 'smth', '0001')
	order2 = Order('UCL',20, 'buy', 100, 'smth', '0001')
	UCLBook.add_order(order1)
	UCLBook.add_order(order2)
	assert UCLBook.get_buy()[0].price<UCLBook.get_buy()[1].price

def test_add_order_3():
	UCLBook = Book('UCL')
	order1 = Order('UCL',20, 'buy', 100, 'smth', '0001')
	order2 = Order('UCL',20, 'buy', 200, 'smth', '0001')
	order3 = Order('UCL',20, 'buy', 300, 'smth', '0001')
	UCLBook.add_order(order1)
	UCLBook.add_order(order2)
	UCLBook.add_order(order3)
	assert (UCLBook.get_buy()[0].price<UCLBook.get_buy()[1].price) & (UCLBook.get_buy()[1].price<UCLBook.get_buy()[2].price)

def test_add_order_4():
	UCLBook = Book('UCL')
	order1 = Order('UCL',20, 'buy', 252, 'smth', '0001')
	order2 = Order('UCL',20, 'buy', 100, 'smth', '0001')
	order3 = Order('UCL',20, 'buy', 300, 'smth', '0001')
	order4 = Order('UCL',20, 'buy', 213, 'smth', '0001')
	UCLBook.add_order(order1)
	UCLBook.add_order(order2)
	UCLBook.add_order(order3)
	assert (UCLBook.get_buy()[0].price<UCLBook.get_buy()[1].price) & (UCLBook.get_buy()[1].price<UCLBook.get_buy()[2].price) & (UCLBook.get_buy()[1].price<UCLBook.get_buy()[2].price)


if '__name__' == '__main__':
	test_add_order_0()
	test_add_order_1()
	test_add_order_2()
	test_add_order_3()
	test_add_order_4()
