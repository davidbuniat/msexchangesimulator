"""
@author: GarSing Chow
@author: Neema Kotonya
This module is a test client that sends preprogrammed orders in for use with test_client_a.
This must be used in conjunction with test_client_a.
"""
import sys
import os
import argparse
print(os.path.dirname(os.path.realpath(__file__))[:-5])
sys.path.append(os.path.dirname(os.path.realpath(__file__))[:-5])

import socket
import struct
import time
from src.Server.lse_protocol_decode_client import Decode 
from src.Server.lse_protocol_encode_client import Encode 
from test_data import new_order

class test_client:
    """
    This class contains the test client.
    """
    def __init__(self):
        """
        Initialises the class. Sets the host, port and creates the client's encoder and parser objects.
        """
        self.HOST, self.PORT = "localhost", 9999
        
        self.encoder = Encode()
        self.parser = Decode()
    
    def logon(self):
        """
        Creates a socket, establishes a connection, then calls the encoder logon function, and checks the reply recieved for successful login.
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.HOST, self.PORT))
        self.sock.sendall(self.encoder.logon())
        self.recieve_data()
        self.fail_reason = ''
        if self.message != struct.pack('<i30s', 0, '0'):
            self.fail_reason = self.fail_reason + 'LOGON FAILED'
        
    def recieve_data(self):
        """
        Recieves a message from the server. First by fetching the header and parsing via the parser. Then recieving the full message with the message length form the header. Finally returning the message.
        @rtype: string
        @return: Returns the message recieved.
        """
        header = self.sock.recv(4)
        mesg_length = self.parser.read_header(header)
        if mesg_length > 0:
            self.message = self.parser.set_message(self.sock.recv(mesg_length))
        else:
            self.parser.set_message('')
        self.parser.parse_message()
        return self.message
    
    def uc1a(self):
        """
        Test for use case 1a. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('1a', 1, 0, 2, 2000, 2000, 10000000000)
        data2 = new_order('1aa', 1, 0, 2, 1000, 1000, 10000000000)
        data3 = new_order('1aaa', 1, 0, 2, 7000, 7000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc1a: data sent')
        self.sock.close()
        

    def uc1b(self):
        """
        Test for use case 1b. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('1b', 1, 0, 2, 2000, 2000, 10000000000)
        data2 = new_order('1bb', 1, 0, 2, 1000, 1000, 10000000000)
        data3 = new_order('1bbb', 1, 0, 2, 7000, 7000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc1b: data sent')
        self.sock.close()
        
    def uc2(self):
        """
        Empty function.
        """
        pass
    
    def uc3(self):
        """
        Test for use case 3. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('3', 1, 0, 2, 2000, 2000, 1000000000)
        data2 = new_order('3a', 1, 0, 2, 3000, 3000, 10000000000)
        data3 = new_order('3aa', 1, 0, 2, 1000, 1000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc3: data sent')
        self.sock.close()
    
    def uc4(self):
        """
        Test for use case 4. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('4', 1, 0, 2, 2000, 2000, 10000000000)
        data2 = new_order('4a', 1, 0, 2, 3000, 3000, 10000000000)
        data3 = new_order('4aa', 1, 0, 2, 5000, 5000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc4: data sent')
        self.sock.close()
        #CANCEL REJECT
        
    def uc5(self):
        """
        Test for use case 5. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('5', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('5a', 1, 0, 2, 2000, 2000, 10000000000)
        data3 = new_order('5aa', 1, 0, 2, 8000, 8000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc5: data sent')
        self.sock.close()
    
    def uc6(self):
        """
        Test for use case 6. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('6', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('6a', 1, 0, 2, 100, 100, 10000000000)
        data3 = new_order('6aa', 1, 0, 2, 10900, 10900, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        time.sleep(0.5)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        print('uc6: data sent')
        self.sock.close()
    
    def uc7(self):
        """
        Test for use case 7. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('7', 1, 0, 2, 10000, 10000, 10000000000)
        data2 = new_order('7a', 1, 0, 2, 2000, 2000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        time.sleep(0.5)
        self.sock.sendall(data2.header + data2.message)
        time.sleep(1)
        print('uc7: data sent')
        self.sock.close()
    
    def uc8(self):
        """
        Test for use case 8. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('8', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('8a', 1, 0, 2, 9000, 9000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        time.sleep(1)
        print('uc8: data sent')
        self.sock.close()
    
    def uc9(self):
        """
        Test for use case 9. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('9', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('9a', 1, 0, 2, 500, 500, 10000000000)
        data3 = new_order('9aa', 1, 0, 2, 100, 100, 1000000000)
        data4 = new_order('9aaa', 1, 0, 2, 6400, 6400, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(0.5)
        self.sock.sendall(data4.header + data4.message)
        time.sleep(1)
        print('uc9: data sent')
        self.sock.close()
    
    def uc10(self):
        """
        Test for use case 10. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('10', 1, 0, 2, 7000, 7000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        time.sleep(5)
        print('uc10: data sent')
        self.sock.close()
    
    def uc11(self):
        """
        Test for use case 11. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('11', 1, 0, 2, 8000, 8000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        time.sleep(5)
        print('uc11: data sent')
        self.sock.close()
    
    def uc12(self):
        """
        Test for use case 12. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('12', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('12a', 1, 0, 2, 500, 500, 10000000000)
        data3 = new_order('12aa', 1, 0, 2, 2000, 2000, 1000000000)
        data4 = new_order('12aaa', 1, 0, 2, 2500, 2500, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        time.sleep(1)
        self.sock.sendall(data3.header + data3.message)
        time.sleep(1)
        self.sock.sendall(data4.header + data4.message)
        time.sleep(1)
        print('uc12: data sent')
        self.sock.close()
    
    
    def uc17(self):
        """
        Test for use case 17. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('17', 1, 0, 2, 1000, 1000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        time.sleep(5)
        print('uc17: data sent')
        self.sock.close()
    
    def uc18(self):
        """
        Test for use case 18. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('18', 1, 0, 2, 1000, 1000, 10000000000)
        data2 = new_order('18', 1, 0, 2, 10000, 10000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        self.sock.sendall(data2.header + data2.message)
        time.sleep(1)
        print('uc18: data sent')
        self.sock.close()
    
    def uc19(self):
        """
        Empty function.
        """
        pass
    
    def uc20(self):
        """
        Test for use case 20. Logs on, creates all order objects for sending, then sends messages in the correct order, printing a success message. Finally closing the connection.
        """
        self.logon()
        data1 = new_order('20', 1, 0, 2, 1000, 1000, 10000000000)
        self.sock.sendall(data1.header + data1.message)
        time.sleep(4)
        print('uc20: data sent')
        self.sock.close()
    
    
    def run(self, uc):
        """
        Runs the use case the user specified, or prints an error message if incorrect.
        @type uc: string
        @param uc: The use case the user wishes to run.
        """
        if uc == '1a':
            self.uc1a()
        elif uc =='1b':
            self.uc1b()
        elif uc == '2':
            self.uc2()
        elif uc == '3':
            self.uc3()
        elif uc == '4':
            self.uc4()
        elif uc == '5':
            self.uc5()
        elif uc == '6':
            self.uc6()
        elif uc == '7':
            self.uc7()
        elif uc == '8':
            self.uc8()
        elif uc == '9':
            self.uc9()
        elif uc == '10':
            self.uc10()
        elif uc == '11':
            self.uc11()
        elif uc == '12':
            self.uc12()
        elif uc == '17':
            self.uc17()
        elif uc == '18':
            self.uc18()
        elif uc == '19':
            self.uc19()
        elif uc == '20':
            self.uc20()
        else: 
            print("This use case does not exist.\nPossible values: 1a, 1b, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20.")
        
def main():
    """
    Creates and runs the test client. An argument parser is used to recieve arguments from the user.
    """
    argu_parser = argparse.ArgumentParser()
    argu_parser.add_argument("usecase", help="Enter the use case number here. Possible values: 1a, 1b, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20.")
    args = argu_parser.parse_args()
    client = test_client()
    client.run(args.usecase)
        
if __name__ == "__main__":
    main()
    