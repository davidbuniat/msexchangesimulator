'''
Created on 3 Feb 2015

@author: dav
'''
import unittest
import threading
import time

from src.Book.book import Book
from src.Book.order import Order
from src.matching_engine import MatchingEngine
from src.report import Report

def put_Order(ME, order):
    ME.process_order(order)
    
def cancel_Order(ME, order):
    ME.cancel_order(order)
    
def replace_Order(ME, old_order,new_order):
    ME.replace_order(old_order,new_order)
        

class Test(unittest.TestCase):


    def test_cancell_multithread(self):
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 200, 'sell', 'limit') 
        #('test_Limit_5','','','')
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 4, 100, 'sell', 'limit') 
        #('test_Limit_5','','','')
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 250, 'buy', 'limit') 
        #('test_Limit_5','','','')
    
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=put_Order, args=(ME, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=cancel_Order, args=(ME, order3))
        
        thread_1.start()
        thread_2.start()
        thread_3.start()
        thread_4.start()
        
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 1) & (len(ME.get_book(123).get_buy()) == 0)
    
    def test_cancell_multithread_UC02(self):
        print("--- UC2 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 200, 'sell', 'limit') 
        
        
          
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=cancel_Order, args=(ME, order1))
        
        thread_1.start()
        time.sleep(0.1)
        thread_2.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
            
    def test_cancell_multithread_UC03(self):
        print("--- UC3 ---")
        ME = MatchingEngine()
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 7, 10000, 'buy', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 2000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 6, 3000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 7, 1000, 'sell', 'limit') 
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))    
        thread_2 = threading.Thread(target=put_Order, args=(ME, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        
        thread_5 = threading.Thread(target=cancel_Order, args=(ME, order1))
        
        
        
        thread_1.start()
        thread_2.start()
        
        time.sleep(0.1)
        
        thread_3.start()
        thread_4.start()
        
        
        thread_5.start() 

        time.sleep(0.000000001)
        # Cancel order 1
         
        time.sleep(0.1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)

    def test_cancell_multithread_UC04(self):
        print("--- UC4 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 7, 10000, 'buy', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 2000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 6, 3000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 7, 5000, 'sell', 'limit') 
        
        
          
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))    
        thread_2 = threading.Thread(target=put_Order, args=(ME, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        
        thread_5 = threading.Thread(target=cancel_Order, args=(ME, order1))

        
        thread_1.start()
        thread_2.start()
        
        time.sleep(0.1)
        
        thread_3.start()
        thread_4.start()
        #time.sleep(0.000000001)
        thread_5.start() 
        
        # Cancel order 1
         
        time.sleep(0.1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)

    def test_cancell_multithread_UC05(self):
        print("--- UC5 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 11000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 
        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        
        thread_1.start()
        time.sleep(0.1)
        thread_2.start()
        thread_3.start()
        thread_4.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 1) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC06(self):
        print("--- UC6 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 12000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 100, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 10900, 'buy', 'limit') 
        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        
        thread_1.start()
        thread_3.start()
        
        time.sleep(0.1)
        thread_2.start()
        thread_4.start()
        thread_5.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)

    def test_cancell_multithread_UC07(self):
        print("--- UC7 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 12000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 10000, 'buy', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 
        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        
        thread_1.start()
        thread_3.start()
        
        time.sleep(0.1)
        thread_2.start()
        thread_4.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
    
    def test_cancell_multithread_UC08(self):
        print("--- UC8 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 4, 10000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 9000, 'buy', 'limit') 
        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        
        thread_1.start()
        thread_3.start()
        
        time.sleep(0.1)
        thread_4.start()
        #time.sleep(0.1)
        thread_2.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC09(self):
        print("--- UC9 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 500, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 100, 'buy', 'limit') 
        
        order6 = Order()
        order6.setID('client_order_id_6', 'trader_id', 123, 'user_id')
        order6.setOrder( 'UCL', 5, 6400, 'buy', 'limit') 
        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        thread_6 = threading.Thread(target=put_Order, args=(ME, order6))
        
        thread_1.start()
        thread_3.start()
        
        time.sleep(0.1)
        thread_4.start()
        thread_2.start()
        thread_5.start()
        thread_6.start()
        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC10(self):
        # Could not be tested as I can't get interleaving
        print("--- UC10 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 7000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 7000, 'buy', 'limit') 

        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        
        thread_1.start()
        
        time.sleep(0.1)
        thread_2.start()
        thread_3.start()

        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC11(self):

        print("--- UC11 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 7000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 8000, 'buy', 'limit') 

        
        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=put_Order, args=(ME, order3))
        
        thread_1.start()
        
        time.sleep(0.1)
        thread_3.start()
        time.sleep(0.1)
        thread_2.start()

        
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC12(self):

        print("--- UC12 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 6000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 500, 'buy', 'limit') 
        
        order6 = Order()
        order6.setID('client_order_id_6', 'trader_id', 123, 'user_id')
        order6.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 
        
        order7 = Order()
        order7.setID('client_order_id_7', 'trader_id', 123, 'user_id')
        order7.setOrder( 'UCL', 5, 2500, 'buy', 'limit') 

        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=replace_Order, args=(ME, order2, order3))
        
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        thread_6 = threading.Thread(target=put_Order, args=(ME, order6))
        thread_7 = threading.Thread(target=put_Order, args=(ME, order7))
        
        thread_1.start()
        thread_4.start()
        
        time.sleep(0.1)
        
        thread_2.start()
        thread_5.start()
        thread_6.start()
        
        time.sleep(0.1)
        thread_3.start()
        thread_7.start()
         
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC13(self):
        # Could not be tested as I can't get interleaving
        print("--- UC13 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 6000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 500, 'buy', 'limit') 
        
        order6 = Order()
        order6.setID('client_order_id_6', 'trader_id', 123, 'user_id')
        order6.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 
        
        order7 = Order()
        order7.setID('client_order_id_7', 'trader_id', 123, 'user_id')
        order7.setOrder( 'UCL', 5, 2500, 'buy', 'limit') 

        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=replace_Order, args=(ME, order2, order3))
        
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        thread_6 = threading.Thread(target=put_Order, args=(ME, order6))
        thread_7 = threading.Thread(target=put_Order, args=(ME, order7))
        
        thread_1.start()
        thread_4.start()
        
        time.sleep(0.1)
        
        thread_2.start()
        thread_5.start()
        thread_6.start()
        
        time.sleep(0.1)
        thread_3.start()
        thread_7.start()
         
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC14(self):
        # Could not be tested as why request order should be cancelled?
        print("--- UC14 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 6000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 500, 'buy', 'limit') 
        
        order6 = Order()
        order6.setID('client_order_id_6', 'trader_id', 123, 'user_id')
        order6.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 
        
        order7 = Order()
        order7.setID('client_order_id_7', 'trader_id', 123, 'user_id')
        order7.setOrder( 'UCL', 5, 1500, 'buy', 'limit') 

        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=replace_Order, args=(ME, order2, order3))
        
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        thread_6 = threading.Thread(target=put_Order, args=(ME, order6))
        thread_7 = threading.Thread(target=put_Order, args=(ME, order7))
        
        thread_1.start()
        thread_4.start()
        
        time.sleep(0.1)
        
        thread_2.start()
        thread_5.start()
        thread_6.start()
        
        time.sleep(0.1)
        thread_3.start()
        thread_7.start()
         
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 1) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC15(self):

        print("--- UC15 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 7000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 6000, 'buy', 'limit') 

        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=replace_Order, args=(ME, order2, order3))
        
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        
        thread_1.start()
        thread_4.start()
        
        time.sleep(0.1)
        
        thread_2.start()
        
        time.sleep(0.3)
        
        thread_3.start()
      
        time.sleep(0.1)
        thread_5.start()  

         
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 0) & (len(ME.get_book(123).get_buy()) == 0)
        
    def test_cancell_multithread_UC16(self):
        # Could not be tested as I doubt there will be interleaving in my concurrent system
        print("--- UC16 ---")
        ME = MatchingEngine()
        
        
        order1 = Order()
        order1.setID('client_order_id_1', 'trader_id', 123, 'user_id')
        order1.setOrder( 'UCL', 5, 10000, 'sell', 'limit') 
        
        order2 = Order()
        order2.setID('client_order_id_2', 'trader_id', 123, 'user_id')
        order2.setOrder( 'UCL', 5, 8000, 'sell', 'limit') 
        
        order3 = Order()
        order3.setID('client_order_id_3', 'trader_id', 123, 'user_id')
        order3.setOrder( 'UCL', 5, 7000, 'sell', 'limit') 
        
        order4 = Order()
        order4.setID('client_order_id_4', 'trader_id', 123, 'user_id')
        order4.setOrder( 'UCL', 5, 1000, 'buy', 'limit') 
        
        order5 = Order()
        order5.setID('client_order_id_5', 'trader_id', 123, 'user_id')
        order5.setOrder( 'UCL', 5, 2000, 'buy', 'limit') 

        
        
        thread_1 = threading.Thread(target=put_Order, args=(ME, order1))
        thread_2 = threading.Thread(target=replace_Order, args=(ME, order1, order2))
        thread_3 = threading.Thread(target=replace_Order, args=(ME, order2, order3))
        
        thread_4 = threading.Thread(target=put_Order, args=(ME, order4))
        thread_5 = threading.Thread(target=put_Order, args=(ME, order5))
        
        thread_1.start()
        thread_4.start()
        
        time.sleep(0.1)
        
        thread_2.start()
        thread_3.start()
      
        time.sleep(0.1)
        thread_5.start()  

         
        time.sleep(1)
        
        assert (len(ME.get_book(123).get_sell()) == 1) & (len(ME.get_book(123).get_buy()) == 0)
   
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
