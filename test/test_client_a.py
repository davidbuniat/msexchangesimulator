"""
@author: GarSing Chow
@author: Neema Kotonya
This module is a test client that sends preprogrammed orders in order to check the replies.
This must be used in conjunction with test_client_a and/or the admin client.
"""
import sys
import os
import argparse
sys.path.append(os.path.dirname(os.path.realpath(__file__))[:-5])

import socket
import time
from src.Server.lse_protocol_decode_client import Decode 
from src.Server.lse_protocol_encode_client import Encode 
from test_data import *

class test_client:
    """
    This class contains the test client.
    """
    
    def __init__(self):
        """
        Initialises the class. Sets the host, port and creates the client's encoder and parser objects.
        """
        self.HOST, self.PORT = "localhost", 9999
        self.encoder = Encode()
        self.parser = Decode()
    
    def logon(self):
        """
        Creates a socket, establishes a connection, then calls the encoder logon function, and checks the reply recieved for successful login.
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((self.HOST, self.PORT))
        except:
            print('Error connecting to server.')
            sys.exit()
        print "\tConnected to server."
        self.sock.sendall(self.encoder.logon())
        self.recieve_data()
        self.fail_reason = ''
        if self.message != struct.pack('<i30s', 0, '0'):
            self.fail_reason = self.fail_reason + 'LOGON FAILED'
            sys.exit()
        
    def recieve_data(self):
        """
        Recieves a message from the server. First by fetching the header and parsing via the parser. Then recieving the full message with the message length form the header. Finally returning the message.
        @rtype: string
        @return: Returns the message recieved.
        """
        header = self.sock.recv(4)
        mesg_length = self.parser.read_header(header)
        if mesg_length > 0:
            self.message = self.parser.set_message(self.sock.recv(mesg_length))
        else:
            self.parser.set_message('')
        self.exec_rep = self.parser.parse_message()
        return self.message
        
    def uc1a(self):
        """
        Test for use case 1a. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc1a: Started')
        self.logon()
        
        UC1a_1 = new_order('x1a', 1, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC1a_1.header + UC1a_1.message) 
        message = self.recieve_data()
        
        UC1a_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC1a_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 8000, 2000, 10000000000)
        UC1a_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 7000, 1000, 10000000000)
        UC1a_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 2, 'F', 0, 7000, 10000000000) 
        
        if message != UC1a_2.message:
            self.fail_reason = self.fail_reason + ' UC1a_2 mismatch'
        if self.recieve_data() != UC1a_3.message:
            self.fail_reason = self.fail_reason + ' UC1a_3 mismatch'
        if self.recieve_data() != UC1a_4.message:
            self.fail_reason = self.fail_reason + ' UC1a_4 mismatch'
        if self.recieve_data() != UC1a_5.message:
            self.fail_reason = self.fail_reason + ' UC1a_5 mismatch'

        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc1a: SUCCESS')
        self.sock.close()

    def uc1b(self):
        """
        Test for use case 1b. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc1b: started')
        self.logon()
        
        UC1b_1 = new_order('x1b', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC1b_1.header + UC1b_1.message) 
        message = self.recieve_data()
        
        UC1b_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC1b_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 8000, 2000, 10000000000)
        UC1b_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 7000, 1000, 10000000000)
        UC1b_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 2, 'F', 0, 7000, 10000000000) 
        
        if message != UC1b_2.message:
            self.fail_reason = self.fail_reason + ' UC1b_2 mismatch'
        if self.recieve_data() != UC1b_3.message:
            self.fail_reason = self.fail_reason + ' UC1b_3 mismatch'
        if self.recieve_data() != UC1b_4.message:
            self.fail_reason = self.fail_reason + ' UC1b_4 mismatch'
        if self.recieve_data() != UC1b_5.message:
            self.fail_reason = self.fail_reason + ' UC1b_5 mismatch'

        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc1b: SUCCESS')
        self.sock.close()
        
    def uc2(self):
        """
        Test for use case 2. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc2: Started')
        self.logon()
        
        UC2_1 = new_order('x1', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC2_1.header + UC2_1.message) 
        message = self.recieve_data()
        
        UC2_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC2_3 = new_order_cancel_request('x2a', 'x2', self.exec_rep['order_id'], 123, 1)
    
        if message != UC2_2.message:
            self.fail_reason = self.fail_reason + ' UC2_2 mismatch'
        self.sock.sendall(UC2_3.header + UC2_3.message)
        message = self.recieve_data()
        UC2_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 4, '4', 0, 0, 0)
        if message != UC2_4.message:
            self.parser.parse_message()
            self.fail_reason = self.fail_reason + ' UC2_4 mismatch'
        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc2: SUCCESS')
        self.sock.close()
        
    def uc3(self):
        """
        Test for use case 3. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc3: Started')
        self.logon()
        
        UC3_1 = new_order('x3', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC3_1.header + UC3_1.message)
        message = self.recieve_data()
        
        UC3_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC3_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 8000, 2000, 10000000000)
        UC3_4 = new_order_cancel_request('x3a', 'x3', self.exec_rep['order_id'], 123, 1)
        UC3_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F', 5000, 3000, 10000000000)
        UC3_7 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F', 4000, 1000, 10000000000)
        
        if message != UC3_2.message:
            self.fail_reason = self.fail_reason + ' UC3_2 mismatch'
        if self.recieve_data() != UC3_3.message:
            self.fail_reason = self.fail_reason + ' UC3_3 mismatch'
         
        time.sleep(1)    
        self.sock.sendall(UC3_4.header + UC3_4.message)
        
        if self.recieve_data() != UC3_5.message:
            self.fail_reason = self.fail_reason + ' UC3_5 mismatch'
        if self.recieve_data() != UC3_7.message:
            self.fail_reason = self.fail_reason + ' UC3_7 mismatch'
        message = self.recieve_data()
        UC3_8 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,4,'4', 0, 0, 0)
        if message != UC3_8.message:
            self.fail_reason = self.fail_reason + ' UC3_8 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc3: SUCCESS')
        self.sock.close()
        
    def uc4(self):
        """
        Test for use case 4. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc4: Started')
        self.logon()
        
        UC4_1 = new_order('x4', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC4_1.header + UC4_1.message)
        message = self.recieve_data()
        
        UC4_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0,'0', 10000, 0, 0 )
        UC4_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1,'F', 8000, 2000, 10000000000)
        UC4_4 = new_order_cancel_request('x4a', 'x4', self.exec_rep['order_id'], 123, 1)
        UC4_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1,'F',5000, 3000, 10000000000)
        UC4_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 2,'F',0, 5000, 10000000000)
        
        if message != UC4_2.message:
            self.fail_reason = self.fail_reason + ' UC4_2 mismatch'
        if self.recieve_data() != UC4_3.message:
            self.fail_reason = self.fail_reason + ' UC4_3 mismatch'
        
        time.sleep(0.1)    
        self.sock.sendall(UC4_4.header + UC4_4.message)
        
        if self.recieve_data() != UC4_5.message:
            self.fail_reason = self.fail_reason + ' UC4_5 mismatch'
        if self.recieve_data() != UC4_6.message:
            self.fail_reason = self.fail_reason + ' UC4_6 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            time.sleep(1)
            print('uc4: SUCCESS')
        self.sock.close()
        
    def uc5(self):
        """
        Test for use case 5. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc5: Started')
        self.logon()
        
        UC5_1 = new_order('x5', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC5_1.header + UC5_1.message) 
        message = self.recieve_data()
        
        UC5_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC5_3 = new_order_cancel_replace_request('x5a', 'x5', self.exec_rep['order_id'], 123, 11000, 11000, 10000000000, 0, 1)
        
        if message != UC5_2.message:
            self.fail_reason = self.fail_reason + ' UC5_2 mismatch'
        self.sock.sendall(UC5_3.header + UC5_3.message)
        message = self.recieve_data()
        
        UC5_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '5', 11000, 0, 0)
        UC5_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 10000, 1000, 10000000000)
        UC5_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 8000, 2000, 10000000000)
        
        if message != UC5_4.message:
            self.fail_reason = self.fail_reason + ' UC5_4 mismatch'
        if self.recieve_data() != UC5_5.message:
            self.fail_reason = self.fail_reason + ' UC5_5 mismatch'
        if self.recieve_data() != UC5_6.message:
            self.fail_reason = self.fail_reason + ' UC5_6 mismatch'
        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            time.sleep(1)
            print('uc5: SUCCESS')
        self.sock.close()
        
    def uc6(self):
        """
        Test for use case 6. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc6: Started')
        self.logon()
        UC6_1 = new_order('x6,', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC6_1.header + UC6_1.message)
        message = self.recieve_data()

        UC6_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000, 0, 0)
        UC6_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',9000,1000, 10000000000)
        UC6_4 = new_order_cancel_replace_request('x6a', 'x6', self.exec_rep['order_id'], 123, 12000, 12000, 10000000000, 0, 1)
        UC6_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',8900,100, 10000000000)

        if message != UC6_2.message:
            self.fail_reason = self.fail_reason + ' UC6_2 mismatch'
        if self.recieve_data() != UC6_3.message:
            self.fail_reason = self.fail_reason + ' UC6_3 mismatch'
        
        time.sleep(0.1)    
        self.sock.sendall(UC6_4.header + UC6_4.message)
        
        if self.recieve_data() != UC6_5.message:
            self.fail_reason = self.fail_reason + ' UC6_5 mismatch'
        
        message = self.recieve_data()
        
        UC6_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'5',10900,0, 0)
        UC6_7 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0, 10900, 10000000000)
        if message != UC6_6.message:
            self.fail_reason = self.fail_reason + ' UC6_6 mismatch'
        if self.recieve_data() != UC6_7.message:
            self.fail_reason = self.fail_reason + ' UC6_7 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc6: SUCCESS')
        self.sock.close()
    
    def uc7(self):
        """
        Test for use case 7. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc7: Started')
        self.logon()
        UC7_1 = new_order('x7', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC7_1.header + UC7_1.message)
        message = self.recieve_data()

        UC7_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000, 0,0)
        UC7_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0, 10000,10000000000)
        UC7_4 = new_order_cancel_replace_request('x7a', 'x7', self.exec_rep['order_id'], 123, 12000, 12000, 10000000000, 0, 1)
        
        if message != UC7_2.message:
            self.fail_reason = self.fail_reason + ' UC7_2 mismatch'
        if self.recieve_data() != UC7_3.message:
            self.fail_reason = self.fail_reason + ' UC7_3 mismatch'
        
        
        time.sleep(0.1)    
        self.sock.sendall(UC7_4.header + UC7_4.message)
        
        message = self.recieve_data()
        UC7_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'5',2000, 0, 0)
        UC7_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0, 2000,10000000000)
        if message != UC7_5.message:
            self.fail_reason = self.fail_reason + ' UC7_5 mismatch'
        if self.recieve_data() != UC7_6.message:
            self.fail_reason = self.fail_reason + ' UC7_6 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc7: SUCCESS')
        self.sock.close()    
    
    def uc8(self):
        """
        Test for use case 8. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc8: Started')
        self.logon()
        UC8_1 = new_order('x8', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC8_1.header + UC8_1.message)
        message = self.recieve_data()

        UC8_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC8_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',9000,1000,10000000000)
        UC8_4 = new_order_cancel_replace_request('x8a', 'x8', self.exec_rep['order_id'], 123, 10000, 10000, 11000000000, 0, 1)
        UC8_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0,9000,10000000000)
        
        
        if message != UC8_2.message:
            self.fail_reason = self.fail_reason + ' UC8_2 mismatch'
        if self.recieve_data() != UC8_3.message:
            self.fail_reason = self.fail_reason + ' UC8_3 mismatch'
            
        time.sleep(0.1)    
        self.sock.sendall(UC8_4.header + UC8_4.message)
        
        if self.recieve_data() != UC8_5.message:
            self.fail_reason = self.fail_reason + ' UC8_5 mismatch'
        
        message = self.recieve_data()
        UC8_6 = new_order_cancel_reject(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 0)
        if message != UC8_6.message:
            self.fail_reason = self.fail_reason + ' UC8_6 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc8: SUCCESS')
        self.sock.close()
     
    def uc9(self):
        """
        Test for use case 9. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc9: Started')
        self.logon()
        UC9_1 = new_order('x9', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC9_1.header + UC9_1.message)
        message = self.recieve_data()

        UC9_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC9_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',9000,1000,10000000000)
        UC9_4 = new_order_cancel_replace_request('x9a', 'x9', self.exec_rep['order_id'], 123, 8000, 8000, 10000000000, 0, 1)
        UC9_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',8500,500,10000000000)
        UC9_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',8400,100,10000000000)
        
        if message != UC9_2.message:
            self.fail_reason = self.fail_reason + ' UC9_2 mismatch'
        if self.recieve_data() != UC9_3.message:
            self.fail_reason = self.fail_reason + ' UC9_3 mismatch'
        
        time.sleep(0.1)    
        self.sock.sendall(UC9_4.header + UC9_4.message)
        
        if self.recieve_data() != UC9_5.message:
            self.fail_reason = self.fail_reason + ' UC9_5 mismatch'
        if self.recieve_data() != UC9_6.message:
            self.fail_reason = self.fail_reason + ' UC9_6 mismatch'
        
        message = self.recieve_data()
        UC9_7 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'5',6400,0,0)
        UC9_8 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0,6400,10000000000)
        if message != UC9_7.message:
            self.fail_reason = self.fail_reason + ' UC9_7 mismatch'
        if self.recieve_data() != UC9_8.message:
            self.fail_reason = self.fail_reason + ' UC9_8 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc9: SUCCESS')
        self.sock.close()
    
    def uc10(self): 
        """
        Test for use case 10. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc10: Started')
        self.logon()
        UC10_1 = new_order('x10', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC10_1.header + UC10_1.message)
        message = self.recieve_data()

        UC10_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC10_3 = new_order_cancel_replace_request('x10a', 'x10', self.exec_rep['order_id'], 123, 7000, 7000, 10000000000, 0, 1)
        UC10_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',3000,7000,10000000000)
        
        
        if message != UC10_2.message:
            self.fail_reason = self.fail_reason + ' UC10_2 mismatch'  
        self.sock.sendall(UC10_3.header + UC10_3.message)
        
        if self.recieve_data() != UC10_4.message:
            self.fail_reason = self.fail_reason + ' UC10_4 mismatch'
         
        message = self.recieve_data()  
        UC10_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'5',0,0,0)
        if message != UC10_5.message:
            self.fail_reason = self.fail_reason + ' UC10_5 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc10: SUCCESS')
        self.sock.close()
        
    def uc11(self):
        """
        Test for use case 11. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc11: Started')
        self.logon()
        UC11_1 = new_order('x11', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC11_1.header + UC11_1.message)
        message = self.recieve_data()

        UC11_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC11_3 = new_order_cancel_replace_request('x11a', 'x11', self.exec_rep['order_id'], 123, 7000, 7000, 10000000000, 0, 1)
        UC11_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',2000,8000,10000000000)
        
        if message != UC11_2.message:
            self.fail_reason = self.fail_reason + ' UC11_2 mismatch'
            
        self.sock.sendall(UC11_3.header + UC11_3.message)
        
        if self.recieve_data() != UC11_4.message:
            self.fail_reason = self.fail_reason + ' UC11_4 mismatch'
            
        message = self.recieve_data()
        UC11_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'5',0,0,0)
        if message != UC11_5.message:
            self.fail_reason = self.fail_reason + ' UC11_5 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc11: SUCCESS')
        self.sock.close()
    
    def uc12(self):
        """
        Test for use case 12. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc12: Started')
        self.logon()
        UC12_1 = new_order('x12', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC12_1.header + UC12_1.message)
        message = self.recieve_data()

        UC12_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC12_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',9000,1000,10000000000)
        UC12_4 = new_order_cancel_replace_request('x12a', 'x12', self.exec_rep['order_id'], 123, 8000, 8000, 10000000000, 0, 1)
        UC12_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',8500,500,10000000000)
        
        
        if message != UC12_2.message:
            self.fail_reason = self.fail_reason + ' UC12_2 mismatch'
        if self.recieve_data() != UC12_3.message:
            self.fail_reason = self.fail_reason + ' UC12_3 mismatch'
        
        time.sleep(0.5)
        self.sock.sendall(UC12_4.header + UC12_4.message)
        
        if self.recieve_data() != UC12_5.message:
            self.fail_reason = self.fail_reason + ' UC12_5 mismatch'
        
        message = self.recieve_data()
        UC12_6 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'5',6500,0,0)
        UC12_7 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',4500,2000,10000000000)
        UC12_8 = new_order_cancel_replace_request('x12b', 'x12a', self.exec_rep['order_id'], 123, 6000, 6000, 10000000000, 0, 1)
        if message != UC12_6.message:
            self.fail_reason = self.fail_reason + ' UC12_6 mismatch'
        if self.recieve_data() != UC12_7.message:
            self.fail_reason = self.fail_reason + ' UC12_7 mismatch'
            
        self.sock.sendall(UC12_8.header + UC12_8.message)
        
        message = self.recieve_data()
        UC12_9 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'5',2500,0,0)
        UC12_10 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,2,'F',0,2500,10000000000)
        if message != UC12_9.message:
            self.fail_reason = self.fail_reason + ' UC12_9 mismatch'
        if self.recieve_data() != UC12_10.message:
            self.fail_reason = self.fail_reason + ' UC12_10 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc12: SUCCESS')
        self.sock.close()
      
      
    def uc17(self):
        """
        Test for use case 17. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc17: Started')
        self.logon()
        UC17_1 = new_order('x17', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC17_1.header + UC17_1.message)
        message = self.recieve_data()
        
        UC17_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,0,'0',10000,0,0)
        UC17_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,1,'F',9000,1000,10000000000)
        UC17_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,4,'4',0,0,0)
        
        if message != UC17_2.message:
            self.fail_reason = self.fail_reason + ' UC17_2 mismatch'
        if self.recieve_data() != UC17_3.message:
            self.fail_reason = self.fail_reason + ' UC17_3 mismatch'
        print("Please cancel the order using the admin client.")
        if self.recieve_data() != UC17_4.message:
            self.fail_reason = self.fail_reason + ' UC17_4 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc17: SUCCESS')
        self.sock.close()
      
    def uc18(self):
        """
        Test for use case 18. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc18: Started')
        self.logon()
        UC18_1 = new_order('x18', 2, 0, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC18_1.header + UC18_1.message)
        message = self.recieve_data()
        
        UC18_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0,0)
        UC18_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F',9000, 1000, 10000000000)
        UC18_4 = new_order('x18', 2, 0, 1, 10000,10000,100)
        

        if message != UC18_2.message:
            self.fail_reason = self.fail_reason + ' UC18_2 mismatch'
        if self.recieve_data() != UC18_3.message:
            self.fail_reason = self.fail_reason + ' UC18_3 mismatch'
        self.sock.sendall(UC18_4.header + UC18_4.message)
        
        message = self.recieve_data()
        UC18_5 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1,8, '8',10000,0,0)
        if message != UC18_5.message:
            self.fail_reason = self.fail_reason + ' UC18_5 mismatch'
            
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc18: SUCCESS')
        self.sock.close()
              
    def uc19(self):
        """
        Test for use case 19. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc19: Started')
        self.logon()
        
        UC19_1 = new_order('x19', 2, 4, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC19_1.header + UC19_1.message) 
        message = self.recieve_data()
        
        UC19_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC19_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 4, '4', 10000, 0, 0)
    
        if message != UC19_2.message:
            self.fail_reason = self.fail_reason + ' UC19_2 mismatch'
        if self.recieve_data() != UC19_3.message:
            self.fail_reason = self.fail_reason + ' UC19_3 mismatch'

        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc19: SUCCESS')
        self.sock.close()
        
    def uc20(self):
        """
        Test for use case 20. Logs on, creates all objects for sending and comparison, then sends and recieves messages in the correct order, checking the messages recieved for correctness, printing an error if found. Finally closing the connection.
        """
        print('uc20: Started')
        self.logon()
        time.sleep(1)
        
        UC20_1 = new_order('x20', 2, 3, 1, 10000, 10000, 10000000000)
        self.sock.sendall(UC20_1.header + UC20_1.message) 
        message = self.recieve_data()
        
        UC20_2 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 0, '0', 10000, 0, 0)
        UC20_3 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 1, 'F', 9000, 1000, 10000000000)
        UC20_4 = new_execution_report(self.exec_rep['client_order_id'], self.exec_rep['order_id'], 1, 4, '4', 9000, 0, 0)
    
        if message != UC20_2.message:
            self.fail_reason = self.fail_reason + ' UC20_2 mismatch'
        if self.recieve_data() != UC20_3.message:
            self.fail_reason = self.fail_reason + ' UC20_3 mismatch'
        if self.recieve_data() != UC20_4.message:
            self.fail_reason = self.fail_reason + ' UC20_4 mismatch'
        
        if self.fail_reason != '':
            print(self.fail_reason)
            self.fail_reason = ''
        else: 
            print('uc20: SUCCESS')
        self.sock.close()
    
          
    def run(self, uc):
        """
        Runs the use case the user specified, or prints an error message if incorrect.
        @type uc: string
        @param uc: The use case the user wishes to run.
        """
        if uc == '1a':
            self.uc1a()
        elif uc =='1b':
            self.uc1b()
        elif uc == '2':
            print("This use case does not require the use of test client b.")
            self.uc2()
        elif uc == '3':
            self.uc3()
        elif uc == '4':
            self.uc4()
        elif uc == '5':
            self.uc5()
        elif uc == '6':
            self.uc6()
        elif uc == '7':
            self.uc7()
        elif uc == '8':
            self.uc8()
        elif uc == '9':
            self.uc9()
        elif uc == '10':
            print("Please ensure you run client B first for this use case.")
            raw_input('Press enter: ')
            self.uc10()
        elif uc == '11':
            print("Please ensure you run client B first for this use case.")
            raw_input('Press enter: ')
            self.uc11()
        elif uc == '12':
            self.uc12()
        elif uc == '17':
            print("Please ensure you run client B first for this use case.")
            raw_input('Press enter: ')
            self.uc17()
        elif uc == '18':
            self.uc18()
        elif uc == '19':
            self.uc19()
        elif uc == '20':
            print("Please ensure you run client B first for this use case.")
            raw_input('Press enter: ')
            self.uc20()
        else: 
            print("This use case does not exist.\nPossible values: 1a, 1b, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20.")
        
def main():
    """
    Creates and runs the test client. An argument parser is used to recieve arguments from the user.
    """
    argu_parser = argparse.ArgumentParser()
    argu_parser.add_argument("usecase", help="Enter the use case number here. Possible values: 1a, 1b, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20.")
    args = argu_parser.parse_args()
    client = test_client()
    client.run(args.usecase)
        
if __name__ == "__main__":
    main()
    