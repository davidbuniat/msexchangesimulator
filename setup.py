from distutils.core import setup

setup(name='MSExchangeSim',
      version='1.0',
      description='Morgan Stanley Exchange Simulator',
      author='Davit Bunityan, GarSing Chow, Neema Kotonya',
      author_email = 'UCL',
      url='-',
      packages=['src', 'src.Book', 'src.Server'],
     )